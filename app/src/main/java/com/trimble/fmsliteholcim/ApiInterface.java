package com.trimble.fmsliteholcim;

/**
 * Created by sbhosal on 10/17/17.
 */

 import com.trimble.fmsliteholcim.entity.MobileAppVersion;
 import com.trimble.fmsliteholcim.entity.RequestVO;
 import com.trimble.fmsliteholcim.entity.Result;
 import com.trimble.fmsliteholcim.entity.User;
 import com.trimble.fmsliteholcim.entity.VehicleTrailDetails;

 import java.util.List;

 import retrofit2.Call;
 import retrofit2.http.Body;
 import retrofit2.http.GET;
 import retrofit2.http.POST;

public interface ApiInterface {

    @POST("post/json/authenticate")
    Call<Result> authenticateUser(@Body User user);

    @POST("post/json/getVehicleTrackingCount")
    Call<Result> getVehicleCounts(@Body RequestVO resultDataVO);

    @GET("get/json/getAppVersion?appCode=5")
    Call<List<MobileAppVersion>> getVersionUpdate();

//    @GET("users?appCode={appCode}")
//    Call<MobileAppVersion> listRepos(@Path("appCode") String appCode);

    @POST("post/json/getTransporters")
    Call<Result> getTransporters(@Body RequestVO resultDataVO);

    //Global Search list
    @POST("post/json/getVehicles")
    Call<Result> getVehicles(@Body RequestVO resultDataVO);

    //Complete Vehicle Details
    @POST("post/json/getVehicleTrackingDetails")
    Call<Result> getVehicleTrackingDetails(@Body RequestVO requestVO);

    //get Vehicle trail
    @POST("post/json/getVehicleTrailDetails")
    Call<Result> getVehicleTrailDetails(@Body VehicleTrailDetails vehicleTrailDetails);





}