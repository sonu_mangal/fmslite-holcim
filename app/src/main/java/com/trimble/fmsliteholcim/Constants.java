package com.trimble.fmsliteholcim;
/**
 * Created by ssarman on 2/26/17.
 */

public interface Constants {

    String SHARED_PREF = "FMSLite_ITL_SHARED_PREF";
    String LAST_REPORTING_TIME = "fonts/SFDigitalReadout-Heavy.ttf";
    String MAp_LOCATION = "http://maps.google.com/maps?q=";       //\" +latitude+\",\"+longitude";
    String VEHICLE_SPEED = "fonts/FasterOne-Regular.ttf";
    String APP_CODE = "3";

    //String GET_CLIENT_ID = "https://"+name+".hpcl.co.in/SSJS/service/user/text/getUseridfromMobileno?mNo=";
    //String ip = "http://10.40.222.66:8001";
    //String ip = "http://210.210.24.99:8080";
    //String ip = "http://210.210.24.162:8085";//ITL Staging
    //String ip = "https://vc.hssaindia.com";//ITL Production
    //String ip = "http://ssjs.tmsitrimble.in/"; //ITL Production
    //String ip = "https://fms.hssaindia.com/FMSLiteServices/fmslite/post/json/authenticate" ;

   //  String ip = "http://ws.tmsitrimble.in";//ITL DNS latest
     //String HUB_IN_OUT_URL="http://210.210.24.163/trakoUnifiedApp/";
   //  String HUB_IN_OUT_URL="https://fms.tmsitrimble.in/trakoUnifiedApp/";     // ITL
     String HUB_IN_OUT_URL="https://fms.hssaindia.com/trakoUnifiedApp/";


    // Hub-in Hub-out
    //String GET_CLIENT_DETAILS = ip+"/SSJS/service/user/text/getClientDetailsV2?clientId=";
   // String GET_CLIENT_ID = ip+"/SSJS/service/user//text/authenticateV2?username=";

    String SHOW_TRIP_LIST = HUB_IN_OUT_URL + "getAmazonTripData.do?userId=";
    String GET_HUB_DATA = HUB_IN_OUT_URL + "getAmazonInputs.do?userId=";
    String CREATE_TRIP_URL = HUB_IN_OUT_URL + "createAmazonTrips.do";

    String LIST = "PASSING_LIST";
    String USERNAME = "username";
    String CLIENT_ID = "clientID";
    String isLoggedin = "isLoggedIn";
    String PASSWORD = "password";
    String REMEMBER_ME = "rememberMe";

    // Hub-In Hub-Out
    String CREATE_TRIP= "createTrip";
    String DISPLAY_TRIP="display";
    String CREATE_TRIP_FLAG= "createTripFlag";
    String DISPLAY_TRIP_FLAG="displayFlag";
    String SELECTED_COUNT = "selectedCount";
   // String GET_TRANSPORTER_LIST = ip+"/SSJS/service/user//text/getTransporterList";
    String VERSION_NAME = "versionName";
    String TYPE = "type";
    String TRANSPORTER_ID = "transporterId";
    String VEHICLE_DETAILS = "vehicle_details";
}
