package com.trimble.fmsliteholcim.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by smangal on 6/15/2017.
 */

public class TripsList {
    List<Trips> tripsList = new ArrayList<>();
    public List<Trips> getTripsList() {
        return tripsList;
    }
    public void setTripsList(List<Trips> tripsList) {
        this.tripsList = tripsList;
    }
}
