package com.trimble.fmsliteholcim.entity;

public class MobileMenuMaster {
	private String menuId;
	private String menuName;
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
}
