package com.trimble.fmsliteholcim.entity;

public class VehicleTrackingCounts {
	
	private Integer totalVehicles;
	private Integer running;
	private Integer stopped;
	private Integer nrd;

	public Integer getNrd() {
		return nrd;
	}

	public void setNrd(Integer nrd) {
		this.nrd = nrd;
	}

	public Integer getTotalVehicles() {
		return totalVehicles;
	}
	public void setTotalVehicles(Integer totalVehicles) {
		this.totalVehicles = totalVehicles;
	}
	public Integer getRunning() {
		return running;
	}
	public void setRunning(Integer running) {
		this.running = running;
	}
	public Integer getStopped() {
		return stopped;
	}
	public void setStopped(Integer stopped) {
		this.stopped = stopped;
	}

	

}
