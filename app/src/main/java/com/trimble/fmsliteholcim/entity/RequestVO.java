package com.trimble.fmsliteholcim.entity;

public class RequestVO {

	private String userId;
	private String transporterId;
	private String vehicleId;
	private String vehicleNumber;
	private String isNRD;
	private String isStopped;
	private String isRunning;
	private Integer isEncrypted;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTransporterId() {
		return transporterId;
	}
	public void setTransporterId(String transporterId) {
		this.transporterId = transporterId;
	}
	public String getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getIsNRD() {
		return isNRD;
	}
	public void setIsNRD(String isNRD) {
		this.isNRD = isNRD;
	}
	public String getIsStopped() {
		return isStopped;
	}
	public void setIsStopped(String isStopped) {
		this.isStopped = isStopped;
	}
	public String getIsRunning() {
		return isRunning;
	}
	public void setIsRunning(String isRunning) {
		this.isRunning = isRunning;
	}
	public Integer getIsEncrypted() {
		return isEncrypted;
	}
	public void setIsEncrypted(Integer isEncrypted) {
		this.isEncrypted = isEncrypted;
	}

}
