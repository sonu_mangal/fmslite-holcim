package com.trimble.fmsliteholcim.entity;


import android.os.Parcel;
import android.os.Parcelable;

public class VehicleTrackingDetails implements Parcelable {
    private String vehicleId;
    private String vehicleNumber;
    private String transporterId;
    private String transporterName;
    private String driverName;
    private String driverMobile;
    private double latitude;
    private double longitude;
    private String lastLocationDatetime;
    private String lastLocation;
    private String speed;
    private String isNRD;
    private String isStopped;
    private String isRunning;
    private String stoppageDateTime;
    private String stoppageDuration;
	private Integer batteryLevel;
	private Integer signalStrength;
	private Integer fuel;

	public Integer getBatteryLevel() {
		return batteryLevel;
	}

	public void setBatteryLevel(Integer batteryLevel) {
		this.batteryLevel = batteryLevel;
	}

	public Integer getSignalStrength() {
		return signalStrength;
	}

	public void setSignalStrength(Integer signalStrength) {
		this.signalStrength = signalStrength;
	}

	public Integer getFuel() {
		return fuel;
	}

	public void setFuel(Integer fuel) {
		this.fuel = fuel;
	}

	public String getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getTransporterId() {
		return transporterId;
	}
	public void setTransporterId(String transporterId) {
		this.transporterId = transporterId;
	}
	public String getTransporterName() {
		return transporterName;
	}
	public void setTransporterName(String transporterName) {
		this.transporterName = transporterName;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getDriverMobile() {
		return driverMobile;
	}
	public void setDriverMobile(String driverMobile) {
		this.driverMobile = driverMobile;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public String getLastLocationDatetime() {
		return lastLocationDatetime;
	}
	public void setLastLocationDatetime(String lastLocationDatetime) {
		this.lastLocationDatetime = lastLocationDatetime;
	}
	public String getLastLocation() {
		return lastLocation;
	}
	public void setLastLocation(String lastLocation) {
		this.lastLocation = lastLocation;
	}
	public String getSpeed() {
		return speed;
	}
	public void setSpeed(String speed) {
		this.speed = speed;
	}
	public String getIsNRD() {
		return isNRD;
	}
	public void setIsNRD(String isNRD) {
		this.isNRD = isNRD;
	}
	public String getIsStopped() {
		return isStopped;
	}
	public void setIsStopped(String isStopped) {
		this.isStopped = isStopped;
	}
	public String getIsRunning() {
		return isRunning;
	}
	public void setIsRunning(String isRunning) {
		this.isRunning = isRunning;
	}
	public String getStoppageDateTime() {
		return stoppageDateTime;
	}
	public void setStoppageDateTime(String stoppageDateTime) {
		this.stoppageDateTime = stoppageDateTime;
	}
	public String getStoppageDuration() {
		return stoppageDuration;
	}
	public void setStoppageDuration(String stoppageDuration) {
		this.stoppageDuration = stoppageDuration;
	}


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.vehicleId);
		dest.writeString(this.vehicleNumber);
		dest.writeString(this.transporterId);
		dest.writeString(this.transporterName);
		dest.writeString(this.driverName);
		dest.writeString(this.driverMobile);
		dest.writeDouble(this.latitude);
		dest.writeDouble(this.longitude);
		dest.writeString(this.lastLocationDatetime);
		dest.writeString(this.lastLocation);
		dest.writeString(this.speed);
		dest.writeString(this.isNRD);
		dest.writeString(this.isStopped);
		dest.writeString(this.isRunning);
		dest.writeString(this.stoppageDateTime);
		dest.writeString(this.stoppageDuration);
		dest.writeValue(this.batteryLevel);
		dest.writeValue(this.signalStrength);
		dest.writeValue(this.fuel);
	}

	public VehicleTrackingDetails() {
	}

	protected VehicleTrackingDetails(Parcel in) {
		this.vehicleId = in.readString();
		this.vehicleNumber = in.readString();
		this.transporterId = in.readString();
		this.transporterName = in.readString();
		this.driverName = in.readString();
		this.driverMobile = in.readString();
		this.latitude = in.readDouble();
		this.longitude = in.readDouble();
		this.lastLocationDatetime = in.readString();
		this.lastLocation = in.readString();
		this.speed = in.readString();
		this.isNRD = in.readString();
		this.isStopped = in.readString();
		this.isRunning = in.readString();
		this.stoppageDateTime = in.readString();
		this.stoppageDuration = in.readString();
		this.batteryLevel = (Integer) in.readValue(Integer.class.getClassLoader());
		this.signalStrength = (Integer) in.readValue(Integer.class.getClassLoader());
		this.fuel = (Integer) in.readValue(Integer.class.getClassLoader());
	}

	public static final Parcelable.Creator<VehicleTrackingDetails> CREATOR = new Parcelable.Creator<VehicleTrackingDetails>() {
		@Override
		public VehicleTrackingDetails createFromParcel(Parcel source) {
			return new VehicleTrackingDetails(source);
		}

		@Override
		public VehicleTrackingDetails[] newArray(int size) {
			return new VehicleTrackingDetails[size];
		}
	};
}
