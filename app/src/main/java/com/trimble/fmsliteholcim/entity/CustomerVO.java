package com.trimble.fmsliteholcim.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by smangal on 6/19/2017.
 */

public class CustomerVO {
    String  customerName;
    List<String> emailId=new ArrayList<String>();

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public List<String> getEmailId() {
        return emailId;
    }

    public void setEmailId(List<String> emailId) {
        this.emailId = emailId;
    }

    @Override
    public String toString() {
        return getCustomerName();
    }
}
