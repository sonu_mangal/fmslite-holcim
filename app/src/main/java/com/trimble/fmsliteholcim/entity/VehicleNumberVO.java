package com.trimble.fmsliteholcim.entity;

/**
 * Created by sbhosal on 1/10/18.
 */

public class VehicleNumberVO {
    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    private String vehicleNumber;

    @Override
    public String toString() {
        return getVehicleNumber();
    }
}
