package com.trimble.fmsliteholcim.entity;

/**
 * Created by smangal on 6/19/2017.
 */

public class LocationVO {
    String location;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return location;
    }
}
