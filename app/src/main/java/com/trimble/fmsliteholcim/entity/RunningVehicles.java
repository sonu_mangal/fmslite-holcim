package com.trimble.fmsliteholcim.entity;

/**
 * Created by smangal on 1/30/2018.
 */

public class RunningVehicles {
    private String vehicleNumber;
    private double latitude;
    private double longitude;

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
