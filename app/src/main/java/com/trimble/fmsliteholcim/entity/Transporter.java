package com.trimble.fmsliteholcim.entity;

public class Transporter {
	
	private String transporterId;
	private String transporterName;
	public String getTransporterId() {
		return transporterId;
	}
	public void setTransporterId(String transporterId) {
		this.transporterId = transporterId;
	}
	public String getTransporterName() {
		return transporterName;
	}
	public void setTransporterName(String transporterName) {
		this.transporterName = transporterName;
	}

	@Override
	public String toString() {
		return getTransporterName();
	}
}
