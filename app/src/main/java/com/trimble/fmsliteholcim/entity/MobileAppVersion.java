package com.trimble.fmsliteholcim.entity;

public class MobileAppVersion {
	private String app_Name;
	private Integer androidVersion;
	private Integer ios_Version;

	public String getApp_Name() {
		return app_Name;
	}

	public void setApp_Name(String app_Name) {
		this.app_Name = app_Name;
	}

	public Integer getAndroidVersion() {
		return androidVersion;
	}
	public void setAndroidVersion(Integer androidVersion) {
		this.androidVersion = androidVersion;
	}

	public Integer getIos_Version() {
		return ios_Version;
	}

	public void setIos_Version(Integer ios_Version) {
		this.ios_Version = ios_Version;
	}
}
