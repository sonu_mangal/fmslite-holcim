package com.trimble.fmsliteholcim.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by smangal on 6/19/2017.
 */

public class Hub {
    List<CustomerVO> customers = new ArrayList<CustomerVO>();

    public List<VehicleNumberVO> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<VehicleNumberVO> vehicles) {
        this.vehicles = vehicles;
    }

    List<VehicleNumberVO> vehicles = new ArrayList<>();
    List<LocationVO> locations = new ArrayList<LocationVO>();

    public List<CustomerVO> getCustomers() {
        return customers;
    }

    public void setCustomers(List<CustomerVO> customers) {
        this.customers = customers;
    }

    public List<LocationVO> getLocations() {
        return locations;
    }

    public void setLocations(List<LocationVO> locations) {
        this.locations = locations;
    }
}
