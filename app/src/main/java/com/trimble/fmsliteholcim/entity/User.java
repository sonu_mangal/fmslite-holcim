package com.trimble.fmsliteholcim.entity;

import java.util.List;

public class User {
	private String applicationId;
	private String userId;
	private String userName;
	private String password;
	private Integer isEncrypted;
	private List<MobileMenuMaster> mobileMenus ;

	public List<MobileMenuMaster> getMobileMenus() {
		return mobileMenus;
	}
	public void setMobileMenus(List<MobileMenuMaster> mobileMenus) {
		this.mobileMenus = mobileMenus;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getIsEncrypted() {
		return isEncrypted;
	}
	public void setIsEncrypted(Integer isEncrypted) {
		this.isEncrypted = isEncrypted;
	}
}
