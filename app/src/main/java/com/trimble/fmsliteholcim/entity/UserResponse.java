package com.trimble.fmsliteholcim.entity;

import com.google.gson.annotations.SerializedName;

public class UserResponse {

    public boolean isNewUser() {
        return isNewUser;
    }

    public void setNewUser(boolean isNewUser) {
        this.isNewUser = isNewUser;
    }

    public boolean isValidUser() {
        return isValidUser;
    }

    public void setValidUser(boolean isValidUser) {
        this.isValidUser = isValidUser;
    }

    @SerializedName(value = "isNewUser")
    private boolean isNewUser;
    
    @SerializedName(value = "isValidUser")
    private boolean isValidUser;

}
