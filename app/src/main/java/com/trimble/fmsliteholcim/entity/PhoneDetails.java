package com.trimble.fmsliteholcim.entity;

import com.google.gson.annotations.SerializedName;

public class PhoneDetails {

    @SerializedName(value = "mobileNumber")
    private String mobileNumber;

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getImeiNumber() {
        return imeiNumber;
    }

    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    @SerializedName(value = "imeiNumber")
    private String imeiNumber;
    @SerializedName(value = "otp")
    private String otp;

    public String getSimCardNumber() {
        return simCardNumber;
    }

    public void setSimCardNumber(String simCardNumber) {
        this.simCardNumber = simCardNumber;
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    @SerializedName(value = "simCardNumber")
    private String simCardNumber;
    @SerializedName(value = "appCode")
    private String appCode;
}
