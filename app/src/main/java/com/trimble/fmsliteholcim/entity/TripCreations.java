package com.trimble.fmsliteholcim.entity;

/**
 * Created by smangal on 6/15/2017.
 */

public class TripCreations {
    private String customerName ;

    private String sourceLocation;

    private String destinationLocation;

    private String vehicleNumber;

    private String departureTime;

    private String plannedTat;

    private String actualTat;

    private String emailIds;

    private String userId;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getSourceLocation() {
        return sourceLocation;
    }

    public void setSourceLocation(String sourceLocation) {
        this.sourceLocation = sourceLocation;
    }

    public String getDestinationLocation() {
        return destinationLocation;
    }

    public void setDestinationLocation(String destinationLocation) {
        this.destinationLocation = destinationLocation;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getPlannedTat() {
        return plannedTat;
    }

    public void setPlannedTat(String plannedTat) {
        this.plannedTat = plannedTat;
    }

    public String getActualTat() {
        return actualTat;
    }

    public void setActualTat(String actualTat) {
        this.actualTat = actualTat;
    }

    public String getEmailIds() {
        return emailIds;
    }

    public void setEmailIds(String emailIds) {
        this.emailIds = emailIds;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
