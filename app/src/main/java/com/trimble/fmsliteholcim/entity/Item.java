package com.trimble.fmsliteholcim.entity;

import android.view.View;

/**
 * Simple POJO model for example
 */
public class Item {

    private String count;
    private int overspeed;
    private String title;
    private int harshBreaks;
    private int alertCount;
    private String date;
    private String time;

    private View.OnClickListener requestBtnClickListener;

    public Item() {
    }

    public Item(String paraCount, int overspeed, int harshBreaks, String title, int alertCount, String date, String time) {
        this.count = paraCount;
        this.overspeed = overspeed;
        this.title = title;
        this.harshBreaks = harshBreaks;
        this.alertCount = alertCount;
        this.date = date;
        this.time = time;
    }

    public int getOverspeed() {
        return overspeed;
    }

    public void setOverspeed(int overspeed) {
        this.overspeed = overspeed;
    }

    public int getHarshBreaks() {
        return harshBreaks;
    }

    public void setHarshBreaks(int harshBreaks) {
        this.harshBreaks = harshBreaks;
    }

    public int getAlertCount() {
        return alertCount;
    }

    public void setAlertCount(int alertCount) {
        this.alertCount = alertCount;
    }


    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public int getoverspeed() {
        return overspeed;
    }

    public void setoverspeed(int overspeed) {
        this.overspeed = overspeed;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

//    public String getToAddress() {
//        return toAddress;
//    }
//
//    public void setToAddress(String toAddress) {
//        this.toAddress = toAddress;
//    }

    public int getRequestsCount() {
        return alertCount;
    }

    public void setRequestsCount(int requestsCount) {
        this.alertCount = requestsCount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public View.OnClickListener getRequestBtnClickListener() {
        return requestBtnClickListener;
    }

    public void setRequestBtnClickListener(View.OnClickListener requestBtnClickListener) {
        this.requestBtnClickListener = requestBtnClickListener;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (alertCount != item.alertCount) return false;
        if (count != null ? !count.equals(item.count) : item.count != null) return false;
        if (title != null ? !title.equals(item.title) : item.title != null)
            return false;
//        if (toAddress != null ? !toAddress.equals(item.toAddress) : item.toAddress != null)
//            return false;
        if (date != null ? !date.equals(item.date) : item.date != null) return false;
        return !(time != null ? !time.equals(item.time) : item.time != null);
    }

    @Override
    public int hashCode() {
        int result = count != null ? count.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        //result = 31 * result + (toAddress != null ? toAddress.hashCode() : 0);
        result = 31 * result + alertCount;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        return result;
    }

//    /**
//     * @return List of elements prepared for tests
//     */
//    public static ArrayList<Item> getTestingList() {
//        ArrayList<Item> items = new ArrayList<>();
//        items.add(new Item("High", "TAP 76", "Current Location", 3, "TODAY", "05:10 PM"));
//        items.add(new Item("Crit", "TAP 66", "RIL, Kalmboli, Mumbai", 10, "TODAY", "11:10 AM"));
//        items.add(new Item("Low", "TM 3K", "Safex, Andheri, Mumbai", 0, "TODAY", "02:11 PM"));
//        items.add(new Item("Med", "TAP 76", "HMC, Vashi, Mumbai", 8, "TODAY", "4:15 AM"));
//        return items;
//
//    }

}
