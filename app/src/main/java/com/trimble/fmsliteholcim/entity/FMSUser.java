package com.trimble.fmsliteholcim.entity;

import com.google.gson.annotations.SerializedName;

public class FMSUser {

	private String userName;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	private String password;
	private String clientIdResponse;
	public String getClientIdResponse() {
		return clientIdResponse;
	}
	public void setClientIdResponse(String clientIdResponse) {this.clientIdResponse = clientIdResponse;}
}
