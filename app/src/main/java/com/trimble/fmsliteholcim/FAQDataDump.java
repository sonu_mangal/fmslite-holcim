package com.trimble.fmsliteholcim;
/**
 * Created by sbhosal on 5/16/2017.
 */


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FAQDataDump {
    public static HashMap<String, List<String>> getData() {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();

        List<String> basketball = new ArrayList<String>();
        basketball.add("1.The logged in user may not have any data associated to his account. \n2.Still you can clear data using first FAQ and login again");
        basketball.add("Clear Data & Cache : Go to Settings-> Applications-> FSMLite-> Clear Data & Clear Cache ");

        List<String> football = new ArrayList<String>();
        football.add("1.Check if Google Maps is installed in your phone. \n2.Check if you are connected to internet.");

        List<String> contact = new ArrayList<String>();
        contact.add("Toll Free : 1800 123 8762\n"+
        "Working hours : Monday - Saturday: 8:00 AM to 8:00 PM (IST)\n"+
        "Email : trako_care@trimble.com\n");


        List<String> annotations = new ArrayList<String>();
        annotations.add("1.Idle Vehicles : Vehicles whose speed is 0\n2.Moving Vehicles : Vehicles whose speed is above 0\n3.Non-Reporting vehicles : Vehicles that are idle for more than 24 hours");

        //expandableListDetail.put("How to clear app cache and data", cricket);
        expandableListDetail.put("Contact Trako Care", contact);
        expandableListDetail.put("Map not working", football);
        expandableListDetail.put("No data available", basketball);

        expandableListDetail.put("Annotations", annotations);

        return expandableListDetail;
    }
}