package com.trimble.fmsliteholcim;
/**
 * Created by sbhosal on 3/8/2017.
 */

import android.app.Application;
import android.content.Context;

import com.trimble.fmsliteholcim.utils.ConnectivityReceiver;

public class MyApplication extends Application {

    private static MyApplication mInstance;

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    public static final String TAG = MyApplication.class
            .getSimpleName();


    private static Context mContext;

    public static Context getContext() {
        return mContext;
    }

    public static void setContext(Context mContext1) {
        mContext = mContext1;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

}