package com.trimble.fmsliteholcim.adapters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.trimble.fmsliteholcim.R;
import com.trimble.fmsliteholcim.entity.Trips;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by smangal on 6/5/2017.
 */

public class TripsAdapter extends BaseAdapter {

    private List<Trips> values;
    private List<Trips> filteredList;
    private Context context;

    public TripsAdapter(Context context, List<Trips> values) {
        this.context = context;
        this.values = values;
        this.filteredList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_view_trip, parent, false);
        final Trips tripsList = values.get(position);

        TextView tv_vehicle_name = (TextView) rowView.findViewById(R.id.tv_vehicle_number);
        TextView tv_distance=(TextView)rowView.findViewById(R.id.tv_distance);
        TextView tv_vehicle_status=(TextView)rowView.findViewById(R.id.tv_vehicle_status);
        TextView tv_TAT=(TextView)rowView.findViewById(R.id.tv_TAT);
        TextView tv_isDelayed=(TextView)rowView.findViewById(R.id.tv_isDelayed);
        SimpleDateFormat deviceResTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        tv_vehicle_name.setText("Vehicle Number : " + (tripsList.getVehicleNumber()==null ? "" : tripsList.getVehicleNumber()));
        tv_TAT.setText("Actual TAT : " + (tripsList.getActualTat().contains("N/A") ? "N/A" : tripsList.getActualTat()+"Hrs"));
        tv_distance.setText("Distance : "+ (tripsList.getDistance()==null ? "0 Kms " : tripsList.getDistance()+" Kms"));
        Log.d("--Delayed : ",tripsList.getIsDelay()+"");
        tv_isDelayed.setText("Status : " +  (tripsList.getIsDelay()==0?"OnTime":"Delayed"));
        if(tripsList.getActualEndDate().contains("N/A") || tripsList.getActualEndDate().length()<5 ){
            tv_vehicle_status.setText("Open");
            tv_vehicle_status.setTextColor(Color.RED);
        }else{
            tv_vehicle_status.setText("Closed");
           // tv_vehicle_status.setTextColor(android.R.color.holo_green_dark);
        }

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Trip Details")
                        .setCancelable(true)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                builder.setMessage("Vehicle Number : " + (tripsList.getVehicleNumber()==null ? "" : tripsList.getVehicleNumber())+
                "\n"+"Planned Start Time : " + (tripsList.getDepartureTime()==null ? " " : tripsList.getDepartureTime())+
                "\n"+"Actual Start Time : "+ (tripsList.getActualStartDate()==null ? " " : tripsList.getActualStartDate())+
                "\n"+"Actual End Time : " + (tripsList.getActualEndDate()==null ? " " : tripsList.getActualEndDate()) +
                "\n"+"Start Location : " + (tripsList.getSourceLocation()==null ? " " : tripsList.getSourceLocation())+
                "\n"+"End Location : " + (tripsList.getDestinationLocation()==null ? " " : tripsList.getDestinationLocation()) +
                "\n"+"Planned TAT : " + (tripsList.getPlannedTat()==null ? " " : tripsList.getPlannedTat()+"Hrs")+
                "\n"+"Actual TAT : " + (tripsList.getActualTat().contains("N/A") ? "N/A" : tripsList.getActualTat()+"Hrs") +
                "\n"+"Distance : "+ (tripsList.getDistance()==null ? "0 Kms " : tripsList.getDistance()+" Kms") +
                "\n"+"Status : " +  (tripsList.getIsDelay()==0?"OnTime":"Delayed"));
                builder.show();
            }
        });
        return rowView;
    }

    public void setVehicleNumber(List<Trips> vehicleStatus) {
        this.values.clear();
        this.filteredList.clear();
        System.out.print("Flter Adapter---------");
        this.values.addAll(vehicleStatus);
        filteredList.addAll(vehicleStatus);
        notifyDataSetChanged();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        values.clear();
        if (charText.length() == 0) {
            values.addAll(filteredList);
        } else {
            for (Trips trips : filteredList) {
                if (trips.getVehicleNumber().toLowerCase().contains(charText)) {
                    values.add(trips);
                }
            }
        }
        notifyDataSetChanged();
    }
}
