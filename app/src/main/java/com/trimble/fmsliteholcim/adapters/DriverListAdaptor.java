package com.trimble.fmsliteholcim.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cardiomood.android.controls.gauge.SpeedometerGauge;
import com.trimble.fmsliteholcim.Constants;
import com.trimble.fmsliteholcim.R;
import com.trimble.fmsliteholcim.activities.DriverListActivity;
import com.trimble.fmsliteholcim.activities.TrailRouteActivity;
import com.trimble.fmsliteholcim.entity.VehicleTrackingDetails;

import java.util.ArrayList;
import java.util.List;

public class DriverListAdaptor extends BaseAdapter {

    public SparseBooleanArray mCheckStates;
    private List<VehicleTrackingDetails> values;
    private List<VehicleTrackingDetails> filteredList;
    private DriverListActivity context;
    Typeface tf;
    Typeface fontSpeed;
    public DriverListAdaptor(DriverListActivity context, List<VehicleTrackingDetails> values) {
        mCheckStates = new SparseBooleanArray(values.size());
        this.context = context;
        tf = Typeface.createFromAsset(context.getAssets(), Constants.LAST_REPORTING_TIME);
        fontSpeed = Typeface.createFromAsset(context.getAssets(), Constants.VEHICLE_SPEED);
        this.values = values;
        this.filteredList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderItem viewHolder;
        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item_driver, parent, false);
            viewHolder = new ViewHolderItem();
            viewHolder.tv_stoppage_time = (TextView) convertView.findViewById(R.id.tv_stoppage_time);
            viewHolder.tv_vehicle_number = (TextView) convertView.findViewById(R.id.tv_vehicle_number);
            viewHolder.tv_driver_name = (TextView) convertView.findViewById(R.id.tv_driver_name);
            viewHolder.tv_vehicle_speed = (TextView) convertView.findViewById(R.id.tv_vehicle_speed);//viewHolder.tv_vehicle_speed.setTypeface(fontSpeed);
            viewHolder.tv_digital_time = (TextView) convertView.findViewById(R.id.tv_digital_time);viewHolder.tv_digital_time.setTypeface(tf);
            viewHolder.tv_lastlocation_date = (TextView) convertView.findViewById(R.id.tv_lastlocation_date);
            viewHolder.tv_lastlocation_city = (TextView) convertView.findViewById(R.id.tv_lastlocation_city);
            viewHolder.speedometer = (SpeedometerGauge) convertView.findViewById(R.id.speedometer);
            viewHolder.img_vehicle_type = (ImageView)convertView.findViewById(R.id.img_vehicle_type);
            viewHolder.img_battery_type = (ImageView) convertView.findViewById(R.id.img_battery_type);
            //viewHolder.driver_icon = (ImageView) convertView.findViewById(R.id.driver_icon);
            viewHolder.ib_share = (ImageButton) convertView.findViewById(R.id.ib_share);

            viewHolder.speedometer.setLabelConverter(new SpeedometerGauge.LabelConverter() {
                @Override
                public String getLabelFor(double progress, double maxProgress) {
                    return String.valueOf((int) Math.round(1));
                }
            });

            // configure value range and ticks
            viewHolder.speedometer.setMaxSpeed(120);
            viewHolder.speedometer.setMajorTickStep(30);
            viewHolder.speedometer.setMinorTicks(4);

            // Configure value range colors
            viewHolder.speedometer.addColoredRange(0, 40, Color.GREEN);
            viewHolder.speedometer.addColoredRange(40, 80, Color.YELLOW);
            viewHolder.speedometer.addColoredRange(80, 120, Color.RED);

            viewHolder.ib_call = (ImageButton) convertView.findViewById(R.id.ib_call);
            viewHolder.ib_location = (ImageButton) convertView.findViewById(R.id.ib_location);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolderItem) convertView.getTag();
        }

        final VehicleTrackingDetails vehicleTrackingDetails = values.get(position);
        viewHolder.tv_vehicle_number.setText(vehicleTrackingDetails.getVehicleNumber());
        final String phone = vehicleTrackingDetails.getDriverMobile();


        Log.d("Status","NRD "+vehicleTrackingDetails.getIsNRD());
        Log.d("Status","Stopped "+vehicleTrackingDetails.getStoppageDateTime());
        Log.d("Status","Running "+vehicleTrackingDetails.getIsRunning());

        if(vehicleTrackingDetails.getDriverName().equalsIgnoreCase("NA")){
            viewHolder.tv_driver_name.setVisibility(View.INVISIBLE);
        }else{
            viewHolder.tv_driver_name.setVisibility(View.VISIBLE);
            viewHolder.tv_driver_name.setText(vehicleTrackingDetails.getDriverName());
        }

        /*if(vehicleTrackingDetails.getDriverName().equalsIgnoreCase("NA") || vehicleTrackingDetails.getDriverName().length()<=2)
            viewHolder.driver_icon.setVisibility(View.GONE);*/
        if(vehicleTrackingDetails.getIsStopped().equals("1")){
            viewHolder.tv_stoppage_time.setVisibility(View.VISIBLE);
            viewHolder.tv_stoppage_time.setText("Stoppage time - \n"+vehicleTrackingDetails.getStoppageDuration());
        }else {
            viewHolder.tv_stoppage_time.setVisibility(View.GONE);
        }
        viewHolder.speedometer.setSpeed(vehicleTrackingDetails.getSpeed()!=null ? Integer.parseInt(vehicleTrackingDetails.getSpeed()):0);
        viewHolder.tv_vehicle_speed.setText(vehicleTrackingDetails.getSpeed()!=null ? vehicleTrackingDetails.getSpeed()+"kmph" : "0kmph");
        viewHolder.tv_digital_time.setText(vehicleTrackingDetails.getLastLocationDatetime()!=null ? vehicleTrackingDetails.getLastLocationDatetime().substring(10,vehicleTrackingDetails.getLastLocationDatetime().length()):"00:00:00");
        viewHolder.tv_lastlocation_date.setText(vehicleTrackingDetails.getLastLocationDatetime()!=null ? vehicleTrackingDetails.getLastLocationDatetime().substring(0,10):"NA");
        viewHolder.tv_lastlocation_city.setText(vehicleTrackingDetails.getLastLocation());
        viewHolder.tv_lastlocation_city.setPaintFlags(viewHolder.tv_lastlocation_city.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        viewHolder.img_vehicle_type.setImageResource(vehicleTrackingDetails.getIsNRD().equalsIgnoreCase("1")? R.drawable.veh_stopped:vehicleTrackingDetails.getIsRunning().equalsIgnoreCase("1")?(R.drawable.veh_moving) : (R.drawable.veh_nrd));
        Integer bl = new Integer(vehicleTrackingDetails.getBatteryLevel());
        viewHolder.img_battery_type.setImageResource(bl<=0 ? (R.drawable.battery_low) : (bl>0 && bl <=11 ? (R.drawable.battery_medium) : (R.drawable.battery_high)));
        viewHolder.ib_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT,"FMS Lite ");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Vehicle Number "+ vehicleTrackingDetails.getVehicleNumber() + " is at Location \'"+vehicleTrackingDetails.getLastLocation()+ "\' at time :"+vehicleTrackingDetails.getLastLocationDatetime()+"\n\nPlease Click below link to see vehicle on the map-   \n"+Constants.MAp_LOCATION+vehicleTrackingDetails.getLatitude()+","+vehicleTrackingDetails.getLongitude()+"\n\nThanks, \n Trimble Trako Team");
                context.startActivity(Intent.createChooser(shareIntent,"Share Via:"));
            }
        });

        if (phone != null && !phone.equalsIgnoreCase("") && !phone.equalsIgnoreCase("null") &&  !phone.contains("NA")) {
           // tv_driver_mobile.setText("Driver Mobile : " + phone);
            viewHolder.ib_call.setVisibility(View.VISIBLE);
        } else {
            //tv_driver_mobile.setText("Driver Mobile : NA ");
            viewHolder.ib_call.setVisibility(View.INVISIBLE);
        }

        final Double lat = vehicleTrackingDetails.getLatitude();
        final Double lang = vehicleTrackingDetails.getLongitude();

        viewHolder.ib_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phone != null && !phone.equalsIgnoreCase("") && !phone.equalsIgnoreCase("null") &&  !phone.contains("NA")) {
                    Toast.makeText(context, "Making call to " + phone, Toast.LENGTH_SHORT).show();
                    context.callDriver(phone);
                } else {
                    Toast.makeText(context, "Sorry... Driver Number not available", Toast.LENGTH_SHORT).show();
                }
            }
        });
        viewHolder.tv_lastlocation_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lat != null && lang != null) {
                    context.showMap(lat, lang,vehicleTrackingDetails.getLastLocation());
                } else {
                    Toast.makeText(context, "Sorry... Latitude or Longitude is not valid", Toast.LENGTH_SHORT).show();
                }
            }
        });

        viewHolder.ib_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lat != null && lang != null) {
                    //context.showMap(lat, lang,vehicleTrackingDetails.getLastLocation());
                    Intent intent = new Intent(context, TrailRouteActivity.class);
                    intent.putExtra(Constants.VEHICLE_DETAILS,vehicleTrackingDetails);
                    context.startActivity(intent);

                } else {
                    Toast.makeText(context, "Sorry... Latitude or Longitude is not valid", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return convertView;
    }

    static class ViewHolderItem {
        TextView tv_vehicle_number, tv_stoppage_time ;
        TextView tv_last_reported;
        TextView tv_driver_name;
        TextView tv_vehicle_speed;
        TextView tv_digital_time;
        TextView tv_lastlocation_date;
        TextView tv_lastlocation_city;
        SpeedometerGauge speedometer;
        ImageView img_vehicle_type, img_battery_type;
        ImageButton ib_call;
        ImageButton ib_location, ib_share;

    }

    public void setDistributorList(List<VehicleTrackingDetails> flexPackageList) {
        this.values.clear();
        this.filteredList.clear();
        this.values = flexPackageList;
        filteredList.addAll(flexPackageList);
        notifyDataSetChanged();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        values.clear();
        Log.d("TYPE", charText);
        if (charText.length() == 0) {
            values.addAll(filteredList);
        } else {
            for (VehicleTrackingDetails vehicleTrackingDetails : filteredList) {
                if (vehicleTrackingDetails.getVehicleNumber().toLowerCase().contains(charText)) {
                    values.add(vehicleTrackingDetails);
                }
            }
        }
        notifyDataSetChanged();
    }
}
