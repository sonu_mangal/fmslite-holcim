package com.trimble.fmsliteholcim.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.trimble.fmsliteholcim.ApiClient;
import com.trimble.fmsliteholcim.ApiInterface;
import com.trimble.fmsliteholcim.Constants;
import com.trimble.fmsliteholcim.R;
import com.trimble.fmsliteholcim.entity.MobileAppVersion;
import com.trimble.fmsliteholcim.entity.MobileMenuMaster;
import com.trimble.fmsliteholcim.entity.Result;
import com.trimble.fmsliteholcim.entity.User;
import com.trimble.fmsliteholcim.utils.EncryptData;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class LoginActivity extends AnimatedAppCompatActivity {
    private Context context;
    private Button buttonLogin;
    private EditText et_username, et_password;
    private ProgressDialog progressBar;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private LinearLayout activity_pre_login;
    private CheckBox cb_remember_me;
    private int myVersionCode = 0;
    private String myVersionName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        activity_pre_login = (LinearLayout) findViewById(R.id.activity_pre_login);
        sharedPreferences = getSharedPreferences(Constants.SHARED_PREF,Context.MODE_PRIVATE);
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(false);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        buttonLogin = (Button) findViewById(R.id.btnSignIn);
        et_username = (EditText) findViewById(R.id.et_username);
        et_password = (EditText) findViewById(R.id.et_password);
        cb_remember_me = (CheckBox)findViewById(R.id.cb_remember_me);
        //***** AUTO UPDATE START
        context = this;
        TextView tv_app_name_version = (TextView) findViewById(R.id.tv_app_name_version);
        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();
        try {
            myVersionName = packageManager.getPackageInfo(packageName, 0).versionName;
            myVersionCode = packageManager.getPackageInfo(packageName, 0).versionCode;
            tv_app_name_version.setText(getString(R.string.app_name)+" v" + myVersionName);
            if(myVersionCode!=0){
                checkVersionUpdates();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        //********* AUTO UPDATE END

        if (sharedPreferences.getBoolean(Constants.isLoggedin, false)) {
            String client_id = sharedPreferences.getString(Constants.CLIENT_ID, "");
            String username = sharedPreferences.getString(Constants.USERNAME, "");
            String password = sharedPreferences.getString(Constants.PASSWORD, "");
            Boolean rememberMe = sharedPreferences.getBoolean(Constants.REMEMBER_ME, false);
            if (client_id != null && username != null && rememberMe) {
                et_username.setText(username);
                et_password.setText(password);
                cb_remember_me.setChecked(true);
            }
        }

        activity_pre_login.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard(view);
                return false;
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (LoginActivity.super.checkConnection()) {
                    if (et_username.getText().toString().length() == 0 || et_password.getText().toString().length() == 0) {
                        Toast.makeText(LoginActivity.this, "Fields cannot be empty!", Toast.LENGTH_SHORT).show();
                    } else {
                        //loginClicked();
                        authenticateUser();
                    }
                }
            }
        });
    }

    private void authenticateUser(){

        progressBar.setMessage("Loading...");
        progressBar.show();

        User user = new User();
        try {
            String ecryptedUsername = EncryptData.encrypt(et_username.getText().toString());
            String ecryptedPassword = EncryptData.encrypt(et_password.getText().toString());

            user.setUserName(ecryptedUsername);
            user.setPassword(ecryptedPassword);
            user.setIsEncrypted(1);

        } catch (Exception e) {
            e.printStackTrace();
        }

        ApiInterface apiService = ApiClient.getFMSServices(this).create(ApiInterface.class);

        Call<Result> call = apiService.authenticateUser(user);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, retrofit2.Response<Result> response) {
                //int statusCode = response.code();
                progressBar.dismiss();

                Result result = response.body();
                if(result!=null && result.getStatus().equalsIgnoreCase("success")){

                    Gson gson = new Gson();
                    JsonObject jsonObject = gson.toJsonTree(result.getData()).getAsJsonObject();
                    User user = gson.fromJson(jsonObject,User.class);

                    editor = sharedPreferences.edit();
                    editor.putBoolean(Constants.isLoggedin, true);
                    editor.putString(Constants.CLIENT_ID, user.getUserId());
                    editor.putString(Constants.USERNAME, et_username.getText().toString());
                    editor.putString(Constants.PASSWORD, et_password.getText().toString());
                    editor.putBoolean(Constants.REMEMBER_ME, cb_remember_me.isChecked());
                    editor.putString(Constants.VERSION_NAME, myVersionName);

                    editor.putBoolean(Constants.CREATE_TRIP_FLAG,false);
                    editor.putBoolean(Constants.DISPLAY_TRIP_FLAG,false);

                    List<MobileMenuMaster> mobileMenuMasterList = user.getMobileMenus();

                    if(mobileMenuMasterList.size()>0){
                        for(MobileMenuMaster mobileMenuMaster : mobileMenuMasterList) {
                            switch(mobileMenuMaster.getMenuId()){
                                case "1":
                                    editor.putBoolean(Constants.CREATE_TRIP_FLAG,true);
                                    break;
                                case "2":
                                    editor.putBoolean(Constants.DISPLAY_TRIP_FLAG,true);
                                    break;
                            }
                        }
                    }

                    editor.commit();
                    editor.apply();

                    Intent dashboardIntent = new Intent(LoginActivity.this,
                            DashboardDrawerActivity.class);
                    startActivity(dashboardIntent);
                    finish();
                }else{
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Invalid Authentication!",
                            Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                progressBar.dismiss();
                Toast.makeText(LoginActivity.this, "Something went wrong. Try again", Toast.LENGTH_LONG).show();
            }
        });

    }

    private void checkVersionUpdates(){
        progressBar.setMessage("Checking for updated version...");
        progressBar.show();
        ApiInterface apiService = ApiClient.getSRPortalServices().create(ApiInterface.class);
        Call<List<MobileAppVersion>> call = apiService.getVersionUpdate();
        call.enqueue(new Callback<List<MobileAppVersion>>() {
            @Override
            public void onResponse(Call<List<MobileAppVersion>> call, retrofit2.Response<List<MobileAppVersion>> response) {
                int statusCode = response.code();
                progressBar.dismiss();

                List<MobileAppVersion> result = response.body();
                if (response != null && myVersionCode != 0 && result.get(0).getAndroidVersion() > myVersionCode) {
                    final AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setMessage("This version of FMS Lite LH is old. Click UPDATE to get the latest version.")
                            .setCancelable(false)
                            .setPositiveButton("UPDATE", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    final String my_package_name = context.getPackageName();
                                    String url = "";
                                    try {
                                        getPackageManager().getPackageInfo("com.android.vending", 0);
                                        url = "market://details?id=" + my_package_name;
                                    } catch (final Exception e) {
                                        url = "https://play.google.com/store/apps/details?id=" + my_package_name;
                                    }
                                    final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                    startActivity(intent);
                                    finish();
                                }
                            })
                            .setNegativeButton("EXIT", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    finish();
                                }
                            });
                    builder1.setTitle("Update Available !");
                    builder1.create().show();
                }else{
                    Toast.makeText(getApplicationContext(),"This version is update to date...Proceed to login",Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<List<MobileAppVersion>> call, Throwable t) {
                progressBar.dismiss();
                t.printStackTrace();
            }
        });

    }
    @Override
    protected void onPause() {
        super.onPause();
        if(progressBar!=null){
            progressBar.dismiss();
        }
        hideKeyboard(et_password);
    }
    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
