package com.trimble.fmsliteholcim.activities.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.trimble.fmsliteholcim.ApiClient;
import com.trimble.fmsliteholcim.ApiInterface;
import com.trimble.fmsliteholcim.Constants;
import com.trimble.fmsliteholcim.R;
import com.trimble.fmsliteholcim.activities.DriverListActivity;
import com.trimble.fmsliteholcim.activities.custom_view.PieView;
import com.trimble.fmsliteholcim.activities.custom_view.animation.PieAngleAnimation;
import com.trimble.fmsliteholcim.entity.RequestVO;
import com.trimble.fmsliteholcim.entity.Result;
import com.trimble.fmsliteholcim.entity.Transporter;
import com.trimble.fmsliteholcim.entity.VehicleTrackingCounts;

import java.util.HashMap;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;

public class DashboardFragment extends Fragment {

    private Button btn_all_vehicle_count;
    private ImageView dashboard_map_view;
    private String client_id, mobile;
    private ProgressDialog progressBar;
    private boolean flag = true;
    private TextView txtAllVehCount, txtMovingCount, txtIdleCount, txtNWCount;
    private Context context;
    private SharedPreferences sharedPreferences;
    private List<Transporter> transporterList;
    //private SearchableSpinner sp_transporter;
    //private ImageButton btn_get_count;
    private Transporter transporter;
    //private LinearLayout ll_transporter;
    private PieView animatedPie1,animatedPie2,animatedPie3,animatedPie4;
    private int totalCount,movingCount,stoppedCount,nrdCount;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    private int mInterval = 60000; // 1 minute by default, can be changed later
    //private Handler mHandler;
    private boolean mIsRunning;
    private String transporterIdEncrypted;

    public DashboardFragment() {
    }

    Activity activity;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        //setRetainInstance(true);
        activity = getActivity();
        //getActivity().setTitle("Dashboard");
    }

    //private boolean isVisibleToUser = false;
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //this.isVisibleToUser = isVisibleToUser;
        if (isVisibleToUser && activity!=null) {
            activity.setTitle("Dashboard");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_dashboard, container, false);

        //mHandler = new Handler();


        dashboard_map_view = (ImageView)rootView.findViewById(R.id.dashboard_map_view);
        btn_all_vehicle_count = (Button)rootView.findViewById(R.id.btn_all_vehicle_count);
        progressBar = new ProgressDialog(context);
        progressBar.setCancelable(true);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

//        btn_get_count = (ImageButton)rootView.findViewById(R.id.btn_get_count);
//        btn_get_count.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                try {
//                    RequestVO requestVO = new RequestVO();
//                    requestVO.setUserId(encrypt(client_id));
//                    if(sp_transporter.getSelectedItemPosition()>0 && transporter!=null) {
//                        requestVO.setTransporterId(encrypt(transporter.getTransporterId()));
//                    }
//                    getVehicleCounts(requestVO);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        });

//        sp_transporter = (SearchableSpinner)rootView.findViewById(R.id.sp_transporters);
//        sp_transporter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                if(transporterList!=null && transporterList.size()>0) {
//                    transporter = transporterList.get(i);
//                }else{
//                    transporter = null;
//                }
//                try {
//                    RequestVO requestVO = new RequestVO();
//                    requestVO.setUserId(encrypt(client_id));
//                    if(sp_transporter.getSelectedItemPosition()>0 && transporter!=null) {
//                        requestVO.setTransporterId(encrypt(transporter.getTransporterId()));
//                    }
//                    getVehicleCounts(requestVO);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//            }
//        });
//
//        sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
//
//        if (sharedPreferences.getBoolean(Constants.isLoggedin, false)) {
//            client_id = sharedPreferences.getString(Constants.CLIENT_ID, "");
//            mobile = sharedPreferences.getString(Constants.USERNAME, "");
//            if (client_id != null && mobile != null) {

                txtAllVehCount = (TextView) rootView.findViewById(R.id.txtAllVehCount);
                txtMovingCount = (TextView) rootView.findViewById(R.id.txtmoVehCount);
                txtIdleCount = (TextView) rootView.findViewById(R.id.txtidVehCount);
                txtNWCount = (TextView) rootView.findViewById(R.id.txtnVehCount);

                LinearLayout ll_total, ll_moving, ll_idle, ll_nw;
               /* fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                DriverListActivity displayTripFragment = new DriverListActivity();
                fragmentTransaction.replace(R.id.fragment, displayTripFragment);
                fragmentTransaction.commit();*/


        final Intent intent = new Intent(getActivity(), DriverListActivity.class);
                intent.putExtra(Constants.TRANSPORTER_ID, "0");

                ll_total = (LinearLayout) rootView.findViewById(R.id.ll_total);
                ll_moving = (LinearLayout) rootView.findViewById(R.id.ll_moving);
                ll_idle = (LinearLayout) rootView.findViewById(R.id.ll_idle);
                ll_nw = (LinearLayout) rootView.findViewById(R.id.ll_nw);

                //ll_transporter = (LinearLayout)rootView.findViewById(R.id.ll_transporter);


        dashboard_map_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(context, MapViewActivity.class));
                ((DashboardHomeFragment)getParentFragment()).setTab(1);
            }
        });
        btn_all_vehicle_count.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        intent.putExtra(Constants.TYPE,"All");
                        intent.putExtra(Constants.SELECTED_COUNT, totalCount);
                        startActivity(intent);
                    }
                });

                ll_moving.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        intent.putExtra(Constants.TYPE,"Moving");
                        startActivity(intent);
                    }
                });

                ll_idle.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        intent.putExtra(Constants.TYPE,"Idle");
                        startActivity(intent);
                    }
                });

                ll_nw.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        intent.putExtra(Constants.TYPE,"NRD");
                        startActivity(intent);
                    }
                });


                //animatedPie1 = (PieView) rootView.findViewById(R.id.animated_pie_view_1);
                //animatedPie1.setPercentageBackgroundColor(getResources().getColor(R.color.col_trimble_blue_dark));
                //animatedPie1.setTextColor(getResources().getColor(R.color.col_trimble_blue_dark));
                //animatedPie1.setInnerTextVisibility(View.VISIBLE);
                //animatedPie1.setTitleText("All Vehicles");
                //animatedPie1.setTitleTextVisibility(View.VISIBLE);

                animatedPie2 = (PieView) rootView.findViewById(R.id.animated_pie_view_2);
                animatedPie2.setPercentageBackgroundColor(getResources().getColor(R.color.green));
                //animatedPie2.setTextColor(getResources().getColor(R.color.green));
                animatedPie2.setInnerTextVisibility(View.VISIBLE);
                animatedPie2.setTitleText("Moving Vehicles");
                animatedPie2.setTitleTextVisibility(View.VISIBLE);

                animatedPie3 = (PieView) rootView.findViewById(R.id.animated_pie_view_3);
                animatedPie3.setPercentageBackgroundColor(getResources().getColor(R.color.yellow));
               // animatedPie3.setTextColor(getResources().getColor(R.color.yellow));
                animatedPie3.setInnerTextVisibility(View.VISIBLE);

                animatedPie4 = (PieView) rootView.findViewById(R.id.animated_pie_view_4);
                animatedPie4.setPercentageBackgroundColor(getResources().getColor(R.color.red));
               // animatedPie4.setTextColor(getResources().getColor(R.color.red));
                animatedPie4.setInnerTextVisibility(View.VISIBLE);

                //animatedPie1.setMainBackgroundColor(getResources().getColor(R.color.contentDividerLine));
                animatedPie2.setMainBackgroundColor(getResources().getColor(R.color.contentDividerLine));
                animatedPie3.setMainBackgroundColor(getResources().getColor(R.color.contentDividerLine));
                animatedPie4.setMainBackgroundColor(getResources().getColor(R.color.contentDividerLine));

//                animatedPie1.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if(totalCount!=0) {
//                            intent.putExtra(Constants.TRANSPORTER_ID, transporterIdEncrypted);
//                            intent.putExtra(TYPE, "All");
//                            intent.putExtra(SELECTED_COUNT, totalCount);
//                            startActivity(intent);
//                        }
//                    }
//                });
                animatedPie2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(movingCount!=0) {
                            intent.putExtra(Constants.TRANSPORTER_ID, transporterIdEncrypted);
                            intent.putExtra(Constants.TYPE, "Moving");
                            intent.putExtra(Constants.SELECTED_COUNT, movingCount);
                            startActivity(intent);
                        }
                    }
                });
                animatedPie3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(stoppedCount!=0) {
                            intent.putExtra(Constants.TRANSPORTER_ID, transporterIdEncrypted);
                            intent.putExtra(Constants.TYPE, "Idle");
                            intent.putExtra(Constants.SELECTED_COUNT, stoppedCount);
                            startActivity(intent);
                        }
                    }
                });
                animatedPie4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(nrdCount!=0) {
                            intent.putExtra(Constants.TRANSPORTER_ID, transporterIdEncrypted);
                            intent.putExtra(Constants.TYPE, "NRD");
                            intent.putExtra(Constants.SELECTED_COUNT, nrdCount);
                            startActivity(intent);
                        }
                    }
                });

//                VehicleTrackingCounts vehicleCounts = ((DashboardHomeFragment)getParentFragment()).getVehicleCount();
//                setDashboardData(vehicleCounts);

                //callAsynchronousTask(client_id);
                //startRepeatingTask();

//                RequestVO requestVO = new RequestVO();
//                try {
//                    requestVO.setUserId(EncryptData.encrypt(client_id));
//                    //getTransporters(requestVO);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

//            } else {
//                Toast.makeText(getActivity(), "Client ID is not valid. Try again.", Toast.LENGTH_LONG).show();
//                clearDataAndLogin();
//            }
//        } else {
//            clearDataAndLogin();
//        }

        return rootView;
    }
//
//    Runnable mStatusChecker = new Runnable() {
//        @Override
//        public void run() {
//            if (!mIsRunning) {
//                return; // stop when told to stop
//            }
//                        try {
//                            if (((DashboardDrawerActivity)getActivity()).checkConnection()) {
//                                if (flag) {
//                                    progressBar.setMessage("Please Wait.. Getting FMS data from server");
//                                    progressBar.show();
//                                    flag = false;
//                                }
//                                //new GetClientData(handler).execute();
//                                RequestVO requestVO = new RequestVO();
//                                requestVO.setUserId(encrypt(client_id));
//
//                                if(sp_transporter.getSelectedItemPosition()>0 && transporter!=null) {
//                                    requestVO.setTransporterId(encrypt(transporter.getTransporterId()));
//                                }
//                                getVehicleCounts(requestVO);
//                            }
//                        } catch (Exception e) {
//                        }
//
//            mHandler.postDelayed(mStatusChecker, mInterval);
//        }
//    };

//    void startRepeatingTask() {
//        mIsRunning = true;
//        mStatusChecker.run();
//    }
//
//    void stopRepeatingTask() {
//        mIsRunning = false;
//        mHandler.removeCallbacks(mStatusChecker);
//    }


public void getVehicleCounts(RequestVO requestVO){
    transporterIdEncrypted = requestVO.getTransporterId();
    System.out.println("Vehicle COunt : " + new Gson().toJson(requestVO));

    requestVO.setIsEncrypted(1);

    totalCount = 0;
    movingCount = 0;
    stoppedCount = 0;
    nrdCount = 0;

//        animatedPie1.setPercentage(0);
//        animatedPie1.setInnerText(Integer.toString(totalCount));
//        animatedPie1.setTitleText("All Vehicles");

    animatedPie2.setPercentage(0);
    animatedPie2.setInnerText(Integer.toString(movingCount));
    animatedPie2.setTitleText("Moving Vehicles");
    animatedPie2.setMainBackgroundColor(getContext().getResources().getColor(R.color.pie_moving));

    animatedPie3.setPercentage(0);
    animatedPie3.setInnerText(Integer.toString(stoppedCount));
    animatedPie3.setTitleText("Idle Vehicles");
    animatedPie3.setMainBackgroundColor(getContext().getResources().getColor(R.color.pie_idle));

    animatedPie4.setPercentage(0);
    animatedPie4.setInnerText(Integer.toString(nrdCount));
    animatedPie4.setTitleText("NRD Vehicles");
    animatedPie4.setMainBackgroundColor(getContext().getResources().getColor(R.color.pie_nrd));

    progressBar.setMessage("Loading...");
    progressBar.show();
    ApiInterface apiService = ApiClient.getFMSServices(context).create(ApiInterface.class);
    Call<Result> call = apiService.getVehicleCounts(requestVO);
    call.enqueue(new Callback<Result>() {
        @Override
        public void onResponse(Call<Result> call, retrofit2.Response<Result> response) {
            progressBar.dismiss();
            Result result = response.body();
            if (result != null && result.getStatus().equalsIgnoreCase("success")) {
                if (result.getData() != null && result.getData() != "") {
                    Gson gson = new Gson();
                    try {
                        JsonObject jsonObject = gson.toJsonTree(result.getData()).getAsJsonObject();
                        VehicleTrackingCounts vehicleCounts = gson.fromJson(jsonObject, VehicleTrackingCounts.class);
                        setDashboardData(vehicleCounts);
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                Toast toast = Toast.makeText(context, "Something went wrong! Please re-open the app", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                toast.show();
            }
        }

        @Override
        public void onFailure(Call<Result> call, Throwable t) {
            progressBar.hide();
            Toast.makeText(context, "Connection problem ! Try again later.", Toast.LENGTH_LONG).show();
        }
    });
}

    public void setDashboardData(VehicleTrackingCounts vehicleCounts){
        if(vehicleCounts!=null && txtAllVehCount!=null) {
            totalCount = vehicleCounts.getTotalVehicles() != null ? vehicleCounts.getTotalVehicles() : 0;
            movingCount = vehicleCounts.getRunning() != null ? vehicleCounts.getRunning() : 0;
            stoppedCount = vehicleCounts.getStopped() != null ? vehicleCounts.getStopped() : 0;
            nrdCount = vehicleCounts.getNrd() != null ? vehicleCounts.getNrd() : 0;

            btn_all_vehicle_count.setText("Total Vehicles : "+Integer.toString(totalCount));
            txtAllVehCount.setText(Integer.toString(totalCount));
            txtMovingCount.setText(Integer.toString(movingCount));
            txtIdleCount.setText(Integer.toString(stoppedCount));
            txtNWCount.setText(Integer.toString(nrdCount));

            Float perRunning = 0.0f;
            Float perStopped = 0.0f;
            Float perNrd = 0.0f;
            Float perTotal = 0.0f;

            if(totalCount!=0) {
                perTotal = 100.0f;
            }
            try {
                perRunning = (float) ((movingCount * 100) / totalCount);
                perStopped = (float) ((stoppedCount * 100) / totalCount);
                perNrd = (float) ((nrdCount * 100) / totalCount);
            } catch (ArithmeticException e) {

            }
            //animatedPie1.setPercentage(perTotal);
            //animatedPie1.setInnerText(Integer.toString(totalCount));

            animatedPie2.setPercentage(perRunning);
            animatedPie2.setInnerText(Integer.toString(movingCount));

            animatedPie3.setPercentage(perStopped);
            animatedPie3.setInnerText(Integer.toString(stoppedCount));

            animatedPie4.setPercentage(perNrd);
            animatedPie4.setInnerText(Integer.toString(nrdCount));

            //PieAngleAnimation animation1 = new PieAngleAnimation(animatedPie1);
            //animation1.setDuration(3000);
            //animatedPie1.startAnimation(animation1);

            PieAngleAnimation animation2 = new PieAngleAnimation(animatedPie2);
            animation2.setDuration(3000);
            animatedPie2.startAnimation(animation2);

            PieAngleAnimation animation3 = new PieAngleAnimation(animatedPie3);
            animation3.setDuration(3000);
            animatedPie3.startAnimation(animation3);

            PieAngleAnimation animation4 = new PieAngleAnimation(animatedPie4);
            animation4.setDuration(3000);
            animatedPie4.startAnimation(animation4);
        }
    }

    private HashMap<String, String> putData(String name, String purpose) {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("name", name);
        item.put("purpose", purpose);
        return item;
    }

//    public void callAsynchronousTask(final String mobile) {
//        Timer timer = new Timer();
//        TimerTask doAsynchronousTask = new TimerTask() {
//            @Override
//            public void run() {
//                Runnable r1 = new Runnable() {
//                    public void run() {
//                        try {
//                            if (((DashboardDrawerActivity)getActivity()).checkConnection()) {
//                                if (flag) {
//                                    progressBar.setMessage("Please Wait.. Getting FMS data from server");
//                                    progressBar.show();
//                                    flag = false;
//                                }
//                                //new GetClientData(handler).execute();
//                                RequestVO requestVO = new RequestVO();
//                                requestVO.setUserId(encrypt(client_id));
//
//                                if(sp_transporter.getSelectedItemPosition()>0 && transporter!=null) {
//                                    requestVO.setTransporterId(encrypt(transporter.getTransporterId()));
//                                }
//                                getVehicleCounts(requestVO);
//                            }
//                        } catch (Exception e) {
//                        }
//                    }
//                };
//            }
//        };
//        timer.schedule(doAsynchronousTask, 0, 60000); //execute in every 50000 ms
//
//        mHandler.post(r1);
//
//    }

    @Override
    public void onDestroy() {
        //stopRepeatingTask();
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
