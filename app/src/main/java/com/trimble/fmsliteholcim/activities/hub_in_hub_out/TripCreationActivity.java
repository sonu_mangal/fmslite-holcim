package com.trimble.fmsliteholcim.activities.hub_in_hub_out;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.trimble.fmsliteholcim.Constants;
import com.trimble.fmsliteholcim.R;
import com.trimble.fmsliteholcim.activities.custom_view.SearchableSpinner;
import com.trimble.fmsliteholcim.entity.CustomerVO;
import com.trimble.fmsliteholcim.entity.Hub;
import com.trimble.fmsliteholcim.entity.LocationVO;
import com.trimble.fmsliteholcim.entity.TripCreations;
import com.trimble.fmsliteholcim.entity.VehicleNumberVO;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.methods.HttpGet;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.entity.StringEntity;
import ch.boye.httpclientandroidlib.impl.client.DefaultHttpClient;

public class TripCreationActivity extends AppCompatActivity {

    private Button tripCreate_btn;
    private TextView customer_tv, vehicle_tv, locationFrom_tv, locationTo_tv, departureTime_tv, tat_tv;
    private com.trimble.fmsliteholcim.activities.custom_view.SearchableSpinner
             vehicle_sp, locationFrom_sp, locationTo_sp;
    private DatePicker departure_date;
    private EditText tat_ed, et_departureDate;
    private ProgressDialog progressBar;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String userName,getDate,getTime;
    int flag=0;
    AutoCompleteTextView customer_sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_creation);

        tripCreate_btn = (Button) findViewById(R.id.tripCreate_btn);
        customer_tv = (TextView) findViewById(R.id.customer_tv);
        vehicle_tv = (TextView) findViewById(R.id.vehicle_tv);
        locationFrom_tv = (TextView) findViewById(R.id.locationFrom_tv);
        locationTo_tv = (TextView) findViewById(R.id.locationTo_tv);
        customer_sp = (AutoCompleteTextView) findViewById(R.id.customer_sp);
        vehicle_sp = (SearchableSpinner) findViewById(R.id.vehicle_sp);
        locationFrom_sp = (SearchableSpinner) findViewById(R.id.locationFrom_sp);
        locationTo_sp = (SearchableSpinner) findViewById(R.id.locationTo_sp);
        tat_ed = (EditText) findViewById(R.id.tat_ed);
        et_departureDate = (EditText) findViewById(R.id.et_departureDate);
        ImageView img = (ImageView) findViewById(R.id.images);

        sharedPreferences = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = sharedPreferences.edit();
        userName = sharedPreferences.getString(Constants.USERNAME, "");

        final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar newCalendar = Calendar.getInstance();
        final DatePickerDialog fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                getDate=dateFormatter.format(newDate.getTime());
                flag=1;
              //  et_departureDate.setText(dateFormatter.format(newDate.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

       final SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:MM:SS");
       Calendar newCalendar1 = Calendar.getInstance();
       final TimePickerDialog timePickerDialog = new TimePickerDialog(this,new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,int minute) {
                        Calendar newTime = Calendar.getInstance();
                        newTime.set(hourOfDay, minute);
                        getTime=timeFormatter.format(newTime.getTime());
                        flag=0;
                    }
                }, newCalendar1.get(Calendar.HOUR), newCalendar1.get(Calendar.MINUTE), false);



        fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        et_departureDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fromDatePickerDialog.show();
               // if(flag==1)
                timePickerDialog.show();
                et_departureDate.setText((getDate==null?"":getDate)+(getTime==null?"":getTime));
            }
        });

        RelativeLayout activity_device = (RelativeLayout) findViewById(R.id.layout_createtrip);
        activity_device.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard(v);
                return false;
            }
        });

        tripCreate_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tat_ed.getText().toString().length() > 0) {
                    TripCreations tripCreations = new TripCreations();
                    tripCreations.setCustomerName(customer_sp.getText().toString());
                    tripCreations.setSourceLocation(vehicle_sp.getSelectedItem().toString());
                    tripCreations.setDestinationLocation(locationFrom_sp.getSelectedItem().toString());
                    tripCreations.setVehicleNumber(locationTo_sp.getSelectedItem().toString());
                    tripCreations.setDepartureTime(et_departureDate.getText().toString());
                    tripCreations.setPlannedTat(tat_ed.getText().toString());
                    progressBar = new ProgressDialog(TripCreationActivity.this);
                    progressBar.setCancelable(false);
                    progressBar.setMessage("Please Wait...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressBar.show();
                     new CreateNewTrip().execute(tripCreations);
                } else if(locationFrom_sp.getSelectedItemPosition()==locationTo_sp.getSelectedItemPosition()){
                    final AlertDialog.Builder builder = new AlertDialog.Builder(TripCreationActivity.this);
                    builder.setMessage("Please select valid destination location, Destination location must be different from Source location..")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    builder.setTitle("FMS Lite");
                    builder.show();
                }{
                    final AlertDialog.Builder builder = new AlertDialog.Builder(TripCreationActivity.this);
                    builder.setMessage("Please insert all valid details.")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    builder.setTitle("FMS Lite");
                    builder.show();
                }


            }
        });

    }

    @Override
    protected void onResume() {
          super.onResume();
        progressBar =new ProgressDialog(TripCreationActivity.this);
        progressBar.setCancelable(false);
        progressBar.setMessage("Please Wait...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.show();
        new HubData().execute(userName);
    }

    class HubData extends AsyncTask<String, Void, List<Hub>> {
        @Override
        protected List<Hub> doInBackground(String... para) {
            try {
                HttpClient httpClient = new DefaultHttpClient();
                String ip = String.format(Constants.GET_HUB_DATA +para[0]);
                Log.d("Show Data URL",ip);
                HttpGet httpGet = new HttpGet(ip);
                Gson gson = new Gson();
                HttpResponse httpResponse = httpClient.execute(httpGet);
                InputStream inputStream = httpResponse.getEntity().getContent();
                StringBuilder sb = new StringBuilder();
                if (inputStream != null) {
                    BufferedReader rd = new BufferedReader(
                            new InputStreamReader(inputStream));
                    String responseStr = "";
                    if (rd != null) {
                        while ((responseStr = rd.readLine()) != null) {
                            sb.append(responseStr);
                        }
                    }
                }
                List<Hub> hubLists = null;
                try {
                    if (sb != null) {
                        //System.out.println(sb.toString());
                        Type listType = new TypeToken<ArrayList<Hub>>() {}.getType();
                        hubLists = gson.fromJson(sb.toString(), listType);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return hubLists;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final List<Hub> result) {
            super.onPostExecute(result);
            Log.d("Trip ",result.size()+"");
            if (progressBar != null){
                progressBar.dismiss();
            }
            if(result != null && result.size()>0){

                ArrayAdapter<CustomerVO> dataAdapterCustmer = new ArrayAdapter<CustomerVO>(TripCreationActivity.this,
                        android.R.layout.simple_spinner_item, result.get(0).getCustomers());
                dataAdapterCustmer.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                customer_sp.setAdapter(dataAdapterCustmer);

                ArrayAdapter<VehicleNumberVO> dataAdapterVehicle = new ArrayAdapter<>(TripCreationActivity.this,
                        android.R.layout.simple_spinner_item, result.get(0).getVehicles());
                dataAdapterVehicle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                vehicle_sp.setAdapter(dataAdapterVehicle);

                ArrayAdapter<LocationVO> dataAdapterFromLoc = new ArrayAdapter<LocationVO>(TripCreationActivity.this,
                        android.R.layout.simple_spinner_item, result.get(0).getLocations());
                dataAdapterFromLoc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                locationFrom_sp.setAdapter(dataAdapterFromLoc);

                ArrayAdapter<LocationVO> dataAdapterToLoc = new ArrayAdapter<LocationVO>(TripCreationActivity.this,
                        android.R.layout.simple_spinner_item, result.get(0).getLocations());
                dataAdapterToLoc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                locationTo_sp.setAdapter(dataAdapterToLoc);
//                tv_notrip.setVisibility(View.GONE);
//                tripsList=result;
//                filterAdapter.setVehicleNumber(tripsList);
//                displayDeviceDetails();
            }else {
              //  tv_notrip.setVisibility(View.VISIBLE);
            }

        }
    }


    class CreateNewTrip extends AsyncTask<TripCreations, Void, String> {
        @Override
        protected String doInBackground(TripCreations... params) {
            try{
                TripCreations tripCreations = params[0];
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(Constants.CREATE_TRIP);
                Gson gson = new Gson();
                System.out.println(gson.toJson(params[0]));
                StringEntity se = new StringEntity(gson.toJson(tripCreations));
                httpPost.setEntity(se);
                httpPost.setHeader("Content-type", "multipart/form-data");
                HttpResponse httpResponse = httpClient.execute(httpPost);
                InputStream inputStream = httpResponse.getEntity().getContent();
                StringBuilder sb = new StringBuilder();
                if (inputStream != null) {
                    BufferedReader rd = new BufferedReader(
                            new InputStreamReader(inputStream));
                    String responseStr = "";
                    if (rd != null) {
                        while ((responseStr = rd.readLine()) != null) {
                            sb.append(responseStr);
                        }
                    }
                }

                //**************

                return sb.toString();
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String response){
            super.onPostExecute(response);
            progressBar.dismiss();
            progressBar = null;
            if(response!=null && Integer.parseInt(response)==1){
                final AlertDialog.Builder builder = new AlertDialog.Builder(TripCreationActivity.this);
                builder.setMessage("Trip Created Successfully.")
                        .setIcon(R.drawable.notification_icon)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                builder.setTitle("FMS Lite");
                builder.show();
            }else{
                final AlertDialog.Builder builder = new AlertDialog.Builder(TripCreationActivity.this);
                builder.setMessage("Server not Responding.Please Try after some time.")
                        .setIcon(R.drawable.notification_icon)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                builder.setTitle("FMS Lite");
                builder.show();
            }

        }
    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void resetFragment(){
        customer_sp.setText("");

    }
}