package com.trimble.fmsliteholcim.activities.hub_in_hub_out;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.trimble.fmsliteholcim.Constants;
import com.trimble.fmsliteholcim.R;
import com.trimble.fmsliteholcim.adapters.TripsAdapter;
import com.trimble.fmsliteholcim.entity.Trips;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.methods.HttpGet;
import ch.boye.httpclientandroidlib.impl.client.DefaultHttpClient;

public class DisplayTripActivity extends AppCompatActivity {

    private EditText et_vehicle;
    private ListView lv_trips;
    private ProgressDialog progressBar;
    private TripsAdapter filterAdapter;
    private List<Trips> tripsList = new ArrayList<Trips>();
    private TextView tv_notrip;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_trip);

        et_vehicle=(EditText)findViewById(R.id.et_vehicle);
        lv_trips=(ListView)findViewById(R.id.lv_trips);
        tv_notrip=(TextView)findViewById(R.id.tv_notrip);


        filterAdapter = new TripsAdapter(this, new ArrayList<Trips>());
        LinearLayout activity_device = (LinearLayout) findViewById(R.id.vehicle_layout);
        activity_device.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard(v);
                return false;
            }
        });

        if (et_vehicle != null) {
            et_vehicle.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (filterAdapter != null) {
                        filterAdapter.filter(s.toString());
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        tv_notrip.setVisibility(View.GONE);
        progressBar =new ProgressDialog(DisplayTripActivity.this);
        progressBar.setCancelable(false);
        progressBar.setMessage("Please Wait...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.show();
        new DisplayTrips().execute();
    }

    class DisplayTrips extends AsyncTask<Void, Void, List<Trips>> {
        @Override
        protected List<Trips> doInBackground(Void... para) {
            try {
                HttpClient httpClient = new DefaultHttpClient();
                String ip = String.format(Constants.SHOW_TRIP_LIST );
                Log.d("Show Trip URL",ip);
                HttpGet httpGet = new HttpGet(ip);
                Gson gson = new Gson();
                HttpResponse httpResponse = httpClient.execute(httpGet);
                InputStream inputStream = httpResponse.getEntity().getContent();
                StringBuilder sb = new StringBuilder();
                if (inputStream != null) {
                    BufferedReader rd = new BufferedReader(
                            new InputStreamReader(inputStream));
                    String responseStr = "";
                    if (rd != null) {
                        while ((responseStr = rd.readLine()) != null) {
                            sb.append(responseStr);
                        }
                    }
                }

                List<Trips> tripLists = null;
                try {
                //[{"sDeviceSerialNumber":"B3300229","srNumber":"6","masterAccount":"IPV","requestDate":"2011-09-27 09:58:00.0"}]
                if (sb != null) {
                    System.out.println(sb.toString());
                    Type listType = new TypeToken<ArrayList<Trips>>() {}.getType();
                    tripLists = gson.fromJson(sb.toString(), listType);
                }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tripLists;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final List<Trips> result) {
            super.onPostExecute(result);
            Log.d("Trip ",result.size()+"");
            if (progressBar != null) {
                progressBar.dismiss();
            }

            if(result !=null && result.size()>0){
                tv_notrip.setVisibility(View.GONE);
                tripsList=result;
                filterAdapter.setVehicleNumber(tripsList);
                displayDeviceDetails();
            }else {
                tv_notrip.setVisibility(View.VISIBLE);
            }

        }
    }

    public void displayDeviceDetails(){
        System.out.print("Filter method called*********");

        lv_trips.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState !=0) {
                    hideKeyboard(view);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
        lv_trips.setAdapter(filterAdapter);
    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
