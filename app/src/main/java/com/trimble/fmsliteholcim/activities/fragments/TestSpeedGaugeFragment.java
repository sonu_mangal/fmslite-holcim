package com.trimble.fmsliteholcim.activities.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cardiomood.android.controls.gauge.SpeedometerGauge;
import com.trimble.fmsliteholcim.R;
import com.trimble.fmsliteholcim.entity.VehicleTrackingCounts;

public class TestSpeedGaugeFragment extends Fragment {
    private SpeedometerGauge speedometer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_test_speedometer, container, false);
        // Customize SpeedometerGauge
        speedometer = (SpeedometerGauge) rootView.findViewById(R.id.img_speedometer);

        speedometer.setLabelConverter(new SpeedometerGauge.LabelConverter() {
            @Override
            public String getLabelFor(double progress, double maxProgress) {
                return String.valueOf((int) Math.round(1));
            }
        });

        // configure value range and ticks
        speedometer.setMaxSpeed(120);
        speedometer.setMajorTickStep(30);
        speedometer.setMinorTicks(4);
        speedometer.setSpeed(80);

        // Configure value range colors
        speedometer.addColoredRange(0, 40, Color.GREEN);
        speedometer.addColoredRange(40, 80, Color.YELLOW);
        speedometer.addColoredRange(80, 120, Color.RED);

        return rootView;
    }


    public void setDashboardData(VehicleTrackingCounts vehicleCounts) {

    }
}
