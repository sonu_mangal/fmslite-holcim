package com.trimble.fmsliteholcim.activities.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.trimble.fmsliteholcim.ApiClient;
import com.trimble.fmsliteholcim.ApiInterface;
import com.trimble.fmsliteholcim.Constants;
import com.trimble.fmsliteholcim.R;
import com.trimble.fmsliteholcim.activities.LoginActivity;
import com.trimble.fmsliteholcim.activities.custom_view.SearchableSpinner;
import com.trimble.fmsliteholcim.entity.RequestVO;
import com.trimble.fmsliteholcim.entity.Result;
import com.trimble.fmsliteholcim.entity.Transporter;
import com.trimble.fmsliteholcim.entity.VehicleTrackingCounts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

import static com.trimble.fmsliteholcim.utils.EncryptData.encrypt;

public class DashboardHomeFragment extends Fragment {

    private Context context;
    private ProgressDialog progressBar;
    private SharedPreferences sharedPreferences;
    private List<Transporter> transporterList;
    private SearchableSpinner sp_transporter;
    private Transporter transporter;
    private VehicleTrackingCounts vehicleCounts;
    private ViewPager viewPager;
    private TabLayout tabs;
    private String transporterId="-1";
    private RequestVO requestVO;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        progressBar = new ProgressDialog(context);
        progressBar.setCancelable(true);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        //setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dashboard_home, container, false);
        // Setting ViewPager for each Tabs
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        // Set Tabs inside Toolbar
        tabs = (TabLayout) view.findViewById(R.id.result_tabs);
        tabs.setupWithViewPager(viewPager);

        getActivity().setTitle("Dashboard");

        final String client_id, mobile;
        if (sharedPreferences.getBoolean(Constants.isLoggedin, false)) {
            try {
                mobile = sharedPreferences.getString(Constants.USERNAME, "");
                if (sharedPreferences.getString(Constants.CLIENT_ID, "") != null && mobile != null) {
                client_id = encrypt(sharedPreferences.getString(Constants.CLIENT_ID, ""));

                    requestVO = new RequestVO();
                    requestVO.setUserId(client_id);

                    sp_transporter = (SearchableSpinner) view.findViewById(R.id.sp_transporters);
                    sp_transporter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (transporterList != null && transporterList.size() > 0) {
                                transporter = transporterList.get(i);
                                if(transporter!=null && transporterId!=transporter.getTransporterId()) {
                                    try {
                                        requestVO = new RequestVO();
                                        requestVO.setUserId(client_id);


                                        if (i > 0) {
                                            transporterId = transporter.getTransporterId();
                                            requestVO.setTransporterId(encrypt(transporter.getTransporterId()));
                                        }else{
                                            transporterId = "-1";
                                        }

                                        mapViewFragment.transporterId = transporterId;

                                        if(dashboardFragment!=null) {
                                            dashboardFragment.getVehicleCounts(requestVO);
                                        }

                                        if(mapViewFragment!=null){
                                            mapViewFragment.getVehicleDetails(requestVO);
                                        }
                                        //getVehicleCounts(requestVO);
                                        //getVehicleDetails(requestVO);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                            } else {
                                transporter = null;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });

                    getTransporters(requestVO);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return view;

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getActivity()!=null) {
            getActivity().setTitle("Dashboard");
        }
    }


    private void getTransporters(RequestVO requestVO1) {
        requestVO1.setIsEncrypted(1);

        progressBar.setMessage("Loading...");
        progressBar.show();
        ApiInterface apiService = ApiClient.getFMSServices(context).create(ApiInterface.class);

        Call<Result> call = apiService.getTransporters(requestVO1);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, retrofit2.Response<Result> response) {
                //int statusCode = response.code();
                Result result = response.body();
                if (result != null && result.getStatus().equalsIgnoreCase("success")) {
                    if (result.getData() != null && result.getData() != "") {
                        Gson gson = new Gson();
                        JsonArray jsonArray = gson.toJsonTree(result.getData()).getAsJsonArray();
                        List transporterList1 = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<Transporter>>() {
                        }.getType());
                        if (transporterList1.size() > 1) {
                            transporterList = new ArrayList<Transporter>();
                            Transporter transporter = new Transporter();
                            transporter.setTransporterName("All Transporters");
                            transporterList.add(transporter);
                            transporterList.addAll(transporterList1);
                            ArrayAdapter<Transporter> adapter = new ArrayAdapter<Transporter>(context,
                                    R.layout.spinner_layout, transporterList);
                            adapter.setDropDownViewResource(R.layout.spinner_layout);
                            sp_transporter.setAdapter(adapter);
                            sp_transporter.setVisibility(View.VISIBLE);
                        } else {
                            sp_transporter.setVisibility(View.GONE);
                            mapViewFragment.transporterId="-1";
                            dashboardFragment.getVehicleCounts(requestVO);
                        }
                    }
                    progressBar.hide();
                } else {
                    progressBar.hide();
                    Toast toast = Toast.makeText(context, "Something went wrong! Please re-open the app", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                }

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                progressBar.dismiss();
                Toast.makeText(context, "Connection problem ! Try again later.", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void clearDataAndLogin() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF,
                Context.MODE_PRIVATE);
        sharedPreferences.edit().clear().commit();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        getActivity().finish();
    }

    private HashMap<String, String> putData(String name, String purpose) {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("name", name);
        item.put("purpose", purpose);
        return item;
    }

    // Add Fragments to Tabs
    private DashboardFragment dashboardFragment;
    private MapViewFragment mapViewFragment;

    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getChildFragmentManager());
        dashboardFragment = new DashboardFragment();
        mapViewFragment = MapViewFragment.newInstance(0,"");
        adapter.addFragment(dashboardFragment, "Dashboard");
        adapter.addFragment(mapViewFragment, "Map View");
        viewPager.setAdapter(adapter);
    }

    public VehicleTrackingCounts getVehicleCount() {
        return vehicleCounts;
    }

    public void setTab(int i) {
        viewPager.setCurrentItem(i);
    }


    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
