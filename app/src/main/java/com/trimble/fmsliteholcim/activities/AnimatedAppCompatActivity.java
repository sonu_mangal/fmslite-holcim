package com.trimble.fmsliteholcim.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.trimble.fmsliteholcim.MyApplication;
import com.trimble.fmsliteholcim.R;
import com.trimble.fmsliteholcim.utils.ConnectivityReceiver;

/**
 * Created by swapnil on 29/1/17.
 */
public class AnimatedAppCompatActivity extends AppCompatActivity
        implements ConnectivityReceiver.ConnectivityReceiverListener {

    private AlertDialog alert;

    public boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
        return isConnected;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MyApplication.setContext(this);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Network Not Available !");
        builder.setMessage("Mobile data or WiFi is required to proceed !")
                .setCancelable(false)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        alert = builder.create();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Listen for connectivity change events
        //registerReceiver(receiver, filter);
        MyApplication.getInstance().setConnectivityListener(this);
        //checkConnection();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //unregisterReceiver(receiver);
    }

    private void showSnack(boolean isConnected) {
        if (isConnected) {
            alert.dismiss();
        } else {
            alert.show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(alert.isShowing()){
            alert.dismiss();
            //finish();
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
      //  showSnack(isConnected);
    }
}
