package com.trimble.fmsliteholcim.activities.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.trimble.fmsliteholcim.ApiClient;
import com.trimble.fmsliteholcim.ApiInterface;
import com.trimble.fmsliteholcim.Constants;
import com.trimble.fmsliteholcim.R;
import com.trimble.fmsliteholcim.activities.TrailRouteActivity;
import com.trimble.fmsliteholcim.activities.custom_view.CustomInfoWindowGoogleMap;
import com.trimble.fmsliteholcim.activities.custom_view.SearchableSpinner;
import com.trimble.fmsliteholcim.entity.RequestVO;
import com.trimble.fmsliteholcim.entity.Result;
import com.trimble.fmsliteholcim.entity.Transporter;
import com.trimble.fmsliteholcim.entity.VehicleTrackingDetails;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

import static com.trimble.fmsliteholcim.utils.EncryptData.encrypt;
public class MapViewFragment extends Fragment {
    private TextView tv_total_count,tv_running_count, tv_idle_count, tv_nrd_count;
    private CheckBox cb_all_vehicles, cb_running_vehicles, cb_nrd_vehicles, cb_stopped;
    private MapView mMapView = null;
    private GoogleMap allvehicleMap;  //, runningMap, nrdMap, idleMap ;
    private List<VehicleTrackingDetails> vehicleTrackingDetailsList = new ArrayList<>();
    private String isNRDSelect="", isRunningSelect="", isIdleSelect="", isAllSelect="";
    private boolean isAllVehicleChecked,isAllVehicleUnchecked, hideAllChecked;
    private Context context;
    private ProgressDialog progressBar;
    private SharedPreferences sharedPreferences;
    private String title;
    private int page = 1;
    private List<Transporter> transporterList;
    private Transporter transporter;
    public String transporterId="-1";
    private String client_id="";
    private SearchableSpinner sp_transporter;
    private RequestVO requestVO;
    private boolean isVisible = false;
    private Activity activity;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRetainInstance(true);
        activity = getActivity();
        context = getActivity();
        progressBar = new ProgressDialog(context);
        progressBar.setCancelable(true);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        try {
            client_id = encrypt(sharedPreferences.getString(Constants.CLIENT_ID, ""));
        } catch (Exception e) {
            e.printStackTrace();
        }

        transporterIdTemp = "0";
        transporterId="-1";
        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");
    }

    // newInstance constructor for creating fragment with arguments
    public static MapViewFragment newInstance(int page, String title) {
        MapViewFragment fragmentFirst = new MapViewFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_map_view, container, false);
        cb_all_vehicles = (CheckBox) view.findViewById(R.id.cb_all_vehicles);
        cb_running_vehicles = (CheckBox) view.findViewById(R.id.cb_running_vehicles);
        cb_nrd_vehicles = (CheckBox) view.findViewById(R.id.cb_nrd_vehicles);
        cb_stopped = (CheckBox) view.findViewById(R.id.cb_stopped);
        tv_total_count = (TextView) view.findViewById(R.id.tv_total_count);
        tv_running_count= (TextView) view.findViewById(R.id.tv_running_count);
        tv_idle_count= (TextView) view.findViewById(R.id.tv_idle_count);
        tv_nrd_count= (TextView) view.findViewById(R.id.tv_nrd_count);
        mMapView = (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        //mMapView.onResume(); // needed to get the map to display immediately
        //mMapView.getMapAsync(this);
        requestVO = new RequestVO();
        requestVO.setUserId(client_id);

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                allvehicleMap = mMap;
                allvehicleMap.getUiSettings().setZoomControlsEnabled(true);

                allvehicleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        VehicleTrackingDetails vehicleTrackingDetails = (VehicleTrackingDetails)marker.getTag();
                        Intent intent = new Intent(context, TrailRouteActivity.class);
                        intent.putExtra(Constants.VEHICLE_DETAILS,vehicleTrackingDetails);
                        context.startActivity(intent);
                    }
                });

                if (allvehicleMap != null) {
                    setMarkerOnMapTemp();
                }
                Log.d("MyFragment", "Fragment outside."+page);
                if(page==1){
                    Log.d("MyFragment", "Fragment is visible."+page);
                    sp_transporter = (SearchableSpinner) view.findViewById(R.id.sp_transporters);
                    sp_transporter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            Log.d("MyFragment", "Page Value."+page+"**"+isVisible);

                            allvehicleMap.clear();
                            tv_total_count.setText("");
                            tv_running_count.setText("");
                            tv_idle_count.setText("");
                            tv_nrd_count.setText("");

                            if (transporterList != null && transporterList.size() > 0) {
                                transporter = transporterList.get(i);
                                if(transporter!=null && transporterId!=transporter.getTransporterId()) {
                                    try {
                                        requestVO = new RequestVO();
                                        requestVO.setUserId(client_id);
                                        if (i > 0) {
                                            transporterId = transporter.getTransporterId();
                                            requestVO.setTransporterId(encrypt(transporter.getTransporterId()));
                                        }else{
                                            transporterId = "-1";
                                        }

                                        if(isVisible||page==1) {
                                            getVehicleDetails(requestVO);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else {
                                transporter = null;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });

                    getTransporters(requestVO);

                }
                //getVehicleDetails(requestVO);
            }
        });

        cb_all_vehicles.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isAllSelect = "1";
                    isNRDSelect = "1";
                    isRunningSelect = "1";
                    isIdleSelect="1";
                    isAllVehicleChecked  =true;
                    isAllVehicleUnchecked = false;
                    cb_running_vehicles.setChecked(true);
                    cb_nrd_vehicles.setChecked(true);
                    cb_stopped.setChecked(true);
                    hideAllChecked = false;
                } else {
                    isAllSelect = "0";
                    if(!hideAllChecked){
                        isNRDSelect = "0";
                        isRunningSelect = "0";
                        isIdleSelect="0";
                        isAllVehicleChecked = false;
                        isAllVehicleUnchecked  =true;
                        cb_running_vehicles.setChecked(false);
                        cb_nrd_vehicles.setChecked(false);
                        cb_stopped.setChecked(false);
                        hideAllChecked = true;
                    }
                }
            }
        });

        cb_running_vehicles.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isRunningSelect = "1";
                    if(isIdleSelect.equals("1") && isRunningSelect.equals("1") && isNRDSelect.equals("1"))
                        cb_all_vehicles.setChecked(true);
                } else {

                    if(isIdleSelect.equals("0") && isRunningSelect.equals("1") && isNRDSelect.equals("0") && isAllSelect.equals("0")){
                        isAllVehicleUnchecked = false;
                        isRunningSelect = "0";
                        isAllSelect = "0";
                        setMarkerOnMapTemp();
                        hideAllChecked  =true;
                        cb_all_vehicles.setChecked(false);
                    }else{
                        isRunningSelect = "0";
                        isAllSelect = "0";
                        hideAllChecked  =true;
                        cb_all_vehicles.setChecked(false);
                    }
                }
                if((isIdleSelect.equals("1") && isRunningSelect.equals("1") && isNRDSelect.equals("1") && isAllSelect.equals("1")) || (isIdleSelect.equals("0") && isRunningSelect.equals("0") && isNRDSelect.equals("0") && isAllSelect.equals("0"))){
                    if(isAllVehicleChecked){
                        setMarkerOnMapTemp();
                        if(isIdleSelect.equals("1") && isRunningSelect.equals("1") && isNRDSelect.equals("1"))
                        isAllVehicleChecked = false;
                    }if(isAllVehicleUnchecked){
                        setMarkerOnMapTemp();
                        if(isIdleSelect.equals("0") && isRunningSelect.equals("0") && isNRDSelect.equals("0"))
                            isAllVehicleUnchecked = false;
                    }
                }else{
                    setMarkerOnMapTemp();
                }

               // setMarkerOnMapTemp();

            }
        });

        cb_nrd_vehicles.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isNRDSelect = "1";
                    if(isIdleSelect.equals("1") && isRunningSelect.equals("1") && isNRDSelect.equals("1"))
                        cb_all_vehicles.setChecked(true);
                } else {
                    if(isIdleSelect.equals("0") && isRunningSelect.equals("0") && isNRDSelect.equals("1") && isAllSelect.equals("0")){
                        isAllVehicleUnchecked = false;
                        isNRDSelect = "0";
                        isAllSelect = "0";
                        setMarkerOnMapTemp();
                        hideAllChecked  =true;
                        cb_all_vehicles.setChecked(false);
                    }else{
                        isNRDSelect = "0";
                        isAllSelect = "0";
                        hideAllChecked  =true;
                        cb_all_vehicles.setChecked(false);
                    }
                }
                if((isIdleSelect.equals("1") && isRunningSelect.equals("1") && isNRDSelect.equals("1") && isAllSelect.equals("1")) || (isIdleSelect.equals("0") && isRunningSelect.equals("0") && isNRDSelect.equals("0") && isAllSelect.equals("0"))){
                    if(isAllVehicleChecked){
                        setMarkerOnMapTemp();
                        if(isIdleSelect.equals("1") && isRunningSelect.equals("1") && isNRDSelect.equals("1"))
                            isAllVehicleChecked = false;
                    }if(isAllVehicleUnchecked){
                        setMarkerOnMapTemp();
                        if(isIdleSelect.equals("0") && isRunningSelect.equals("0") && isNRDSelect.equals("0"))
                            isAllVehicleUnchecked = false;
                    }
                }else{
                    setMarkerOnMapTemp();
                }
               // setMarkerOnMap();
            }
        });

        cb_stopped.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isIdleSelect = "1";
                    if(isIdleSelect.equals("1") && isRunningSelect.equals("1") && isNRDSelect.equals("1"))
                        cb_all_vehicles.setChecked(true);
                } else {
                    if(isIdleSelect.equals("1") && isRunningSelect.equals("0") && isNRDSelect.equals("0") && isAllSelect.equals("0")){
                        isAllVehicleUnchecked = false;
                        isIdleSelect = "0";
                        isAllSelect = "0";
                        setMarkerOnMapTemp();
                        hideAllChecked  =true;
                        cb_all_vehicles.setChecked(false);
                    }else{
                        isIdleSelect = "0";
                        isAllSelect = "0";
                        hideAllChecked  =true;
                        cb_all_vehicles.setChecked(false);
                    }


                }
                if((isIdleSelect.equals("1") && isRunningSelect.equals("1") && isNRDSelect.equals("1") && isAllSelect.equals("1")) || (isIdleSelect.equals("0") && isRunningSelect.equals("0") && isNRDSelect.equals("0") && isAllSelect.equals("0"))){
                    if(isAllVehicleChecked){
                        setMarkerOnMapTemp();
                        if(isIdleSelect.equals("1") && isRunningSelect.equals("1") && isNRDSelect.equals("1"))
                            isAllVehicleChecked = false;
                    }if(isAllVehicleUnchecked){
                        setMarkerOnMapTemp();
                        if(isIdleSelect.equals("0") && isRunningSelect.equals("0") && isNRDSelect.equals("0"))
                            isAllVehicleUnchecked = false;
                    }
                }else{
                    setMarkerOnMapTemp();
                }
               // setMarkerOnMap();
            }
        });

        isAllSelect = "1";
        isNRDSelect = "1";
        isRunningSelect = "1";
        isIdleSelect="1";
        isAllVehicleChecked = true;
        isAllVehicleUnchecked = false;
        hideAllChecked = false;

        return view;
    }

    public void setVehicleList(List<VehicleTrackingDetails> vehicleTrackingList) {
        vehicleTrackingDetailsList = vehicleTrackingList;

        int  total_count=0, run_count=0, idle_count=0, nrd_count=0;
        if(vehicleTrackingList !=null){

            cb_all_vehicles.setChecked(true);
            cb_running_vehicles.setChecked(true);
            cb_nrd_vehicles.setChecked(true);
            cb_stopped.setChecked(true);

            for(int i=0;i < vehicleTrackingList.size();i++){
                if(vehicleTrackingList.get(i).getIsRunning().equals("1"))
                    run_count++;
                else if(vehicleTrackingList.get(i).getIsNRD().equals("1"))
                    nrd_count++;
                else if(vehicleTrackingList.get(i).getIsStopped().equals("1"))
                    idle_count++;
                total_count++;
            }
            tv_total_count.setText("("+vehicleTrackingList.size()+")");
            tv_running_count.setText("("+run_count+")");
            tv_idle_count.setText("("+idle_count+")");
            tv_nrd_count.setText("("+nrd_count+")");
        }

        if (allvehicleMap != null) setMarkerOnMapTemp();
    }


    public void setMarkerOnMapTemsp(){
        System.out.println("a**ll-"+isAllSelect+" NRD-"+isNRDSelect+" Run-"+isRunningSelect+" Idle-"+isIdleSelect);
    }

    private void getTransporters(final RequestVO requestVO) {
        requestVO.setIsEncrypted(1);

        //progressBar.setMessage("Loading...");
        //progressBar.show();
        ApiInterface apiService = ApiClient.getFMSServices(context).create(ApiInterface.class);

        Call<Result> call = apiService.getTransporters(requestVO);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, retrofit2.Response<Result> response) {
                //int statusCode = response.code();
                Result result = response.body();
                if (result != null && result.getStatus().equalsIgnoreCase("success")) {
                    if (result.getData() != null && result.getData() != "") {
                        Gson gson = new Gson();
                        JsonArray jsonArray = gson.toJsonTree(result.getData()).getAsJsonArray();
                        List transporterList1 = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<Transporter>>() {
                        }.getType());
                        if (transporterList1.size() > 1) {
                            transporterList = new ArrayList<Transporter>();
                            Transporter transporter = new Transporter();
                            transporter.setTransporterName("All Transporters");
                            transporterList.add(transporter);
                            transporterList.addAll(transporterList1);
                            ArrayAdapter<Transporter> adapter = new ArrayAdapter<Transporter>(context,
                                    R.layout.spinner_layout, transporterList);
                            adapter.setDropDownViewResource(R.layout.spinner_layout);
                            sp_transporter.setAdapter(adapter);
                            sp_transporter.setVisibility(View.VISIBLE);
                        } else {
                            sp_transporter.setVisibility(View.GONE);
                            getVehicleDetails(requestVO);
                        }
                    }
                } else {
                    Toast toast = Toast.makeText(context, "Something went wrong! Please re-open the app", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                //progressBar.dismiss();
                Toast.makeText(context, "Connection problem ! Try again later.", Toast.LENGTH_LONG).show();
            }
        });
    }
    public void setMarkerOnMapTemp() {
        if(allvehicleMap!=null) {
            allvehicleMap.clear();

            LatLng latLng;
            MarkerOptions markerOptionsVehicle = new MarkerOptions();
            VehicleTrackingDetails vehicleTrackingDetails = new VehicleTrackingDetails();
            BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.veh_moving);
            if ((isAllSelect.equals("1") || isIdleSelect.equals("1") || isRunningSelect.equals("1") || isNRDSelect.equals("1"))) {
                int flag = 0;
                if (vehicleTrackingDetailsList != null && vehicleTrackingDetailsList.size() > 0) {

//            ClusterManager<CustomClusterItem> mClusterManager = new ClusterManager<CustomClusterItem>(context, allvehicleMap);
//            allvehicleMap.setOnCameraIdleListener(mClusterManager);
//            allvehicleMap.setOnMarkerClickListener(mClusterManager);
//            allvehicleMap.setOnInfoWindowClickListener(mClusterManager);

                    for (int i = 0; i < vehicleTrackingDetailsList.size(); i++) {
                        vehicleTrackingDetails = vehicleTrackingDetailsList.get(i);

                        latLng = new LatLng(vehicleTrackingDetails.getLatitude(), vehicleTrackingDetails.getLongitude());
                        markerOptionsVehicle = new MarkerOptions().position(latLng);
                        markerOptionsVehicle.title(vehicleTrackingDetails.getVehicleNumber() + " - " + vehicleTrackingDetails.getSpeed() + "Km/h").snippet(vehicleTrackingDetails.getLastLocation() + "\n" + vehicleTrackingDetails.getSpeed() + "Km/h");

                        //markerOptionsVehicle.icon(BitmapDescriptorFactory.fromResource(R.drawable.veh_moving));
                        bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.veh_moving);

                        flag = 0;
                        if ((vehicleTrackingDetails.getIsNRD().equals("1") && isNRDSelect.equals("1")) || (vehicleTrackingDetails.getIsNRD().equalsIgnoreCase("1") && isAllSelect.equals("1")) || (vehicleTrackingDetails.getIsNRD().equalsIgnoreCase("1") && isAllSelect.equals("0") && isNRDSelect.equals("1"))) {
                            flag = 1;
                            //markerOptionsVehicle.icon(BitmapDescriptorFactory.fromResource(R.drawable.veh_stopped_icon));
                            bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.veh_stopped);
                        } else if ((vehicleTrackingDetails.getIsRunning().equals("1") && isRunningSelect.equals("1")) || (vehicleTrackingDetails.getIsRunning().equalsIgnoreCase("1") && isAllSelect.equals("1")) || (vehicleTrackingDetails.getIsRunning().equalsIgnoreCase("1") && isAllSelect.equals("0") && isRunningSelect.equals("1"))) {
                            flag = 1;
                            //markerOptionsVehicle.icon(BitmapDescriptorFactory.fromResource(R.drawable.veh_moving));
                            bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.veh_moving);
                        } else if ((vehicleTrackingDetails.getIsStopped().equals("1") && isIdleSelect.equals("1")) || (vehicleTrackingDetails.getIsStopped().equalsIgnoreCase("1") && isAllSelect.equals("1")) || (vehicleTrackingDetails.getIsStopped().equalsIgnoreCase("1") && isAllSelect.equals("0") && isIdleSelect.equals("1"))) {
                            flag = 1;
                            //markerOptionsVehicle.icon(BitmapDescriptorFactory.fromResource(R.drawable.veh_nrd));
                            bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.veh_nrd);
                        }

                        if (flag == 1) {
                            Bitmap b = bitmapdraw.getBitmap();
                            Bitmap smallMarker = Bitmap.createScaledBitmap(b, 40, 40, false);
                            markerOptionsVehicle.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));

                            Marker m = allvehicleMap.addMarker(markerOptionsVehicle);
                            m.setTag(vehicleTrackingDetails);

                            CustomInfoWindowGoogleMap customInfoWindowGoogleMap = new CustomInfoWindowGoogleMap(getActivity());
                            allvehicleMap.setInfoWindowAdapter(customInfoWindowGoogleMap);
                        }

                        // mClusterManager.addItem(new CustomClusterItem(vehicleTrackingDetails.getLatitude(),vehicleTrackingDetails.getLongitude()));
                    }

                    // mClusterManager.cluster();

                    if (flag == 1) {
                        int midPoint = 0;
                        midPoint = vehicleTrackingDetailsList.size() / 2;
                        LatLng latLng1 = new LatLng(20.5937, 78.9629);
                        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng1).zoom(4).build();
                        allvehicleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }
                }
            }
        }
    }

    public void getVehicleDetails(RequestVO requestVO) {

        allvehicleMap.clear();

        tv_total_count.setText("");
        tv_running_count.setText("");
        tv_idle_count.setText("");
        tv_nrd_count.setText("");

        cb_all_vehicles.setChecked(false);
        cb_running_vehicles.setChecked(false);
        cb_nrd_vehicles.setChecked(false);
        cb_stopped.setChecked(false);

        vehicleTrackingDetailsList = null;

        if(isVisible) {
            System.out.println("Vehicle COunt : " + new Gson().toJson(requestVO));
            requestVO.setIsEncrypted(1);
            progressBar.setMessage("Loading...");
            progressBar.show();
            ApiInterface apiService = ApiClient.getFMSServices(context).create(ApiInterface.class);

            Call<Result> call = apiService.getVehicleTrackingDetails(requestVO);
            call.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, retrofit2.Response<Result> response) {
                    //int statusCode = response.code();
                    Result result = response.body();

                    int size  = response.body().getData().toString().length();

                    System.out.println("SIZE:"+size);
                    //3086868
                    if(size>347355){
                        progressBar.hide();
                        Toast.makeText(context, "Too many vehicles to show on map!", Toast.LENGTH_SHORT).show();
                    }else {

                        if (result != null && result.getStatus().equalsIgnoreCase("success")) {
                            if (result.getData() != null && result.getData() != "") {
                                try {
                                    Gson gson = new Gson();
                                    JsonArray jsonArray = gson.toJsonTree(result.getData()).getAsJsonArray();
                                    final List<VehicleTrackingDetails> vehicleTrackingDetailsList = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<VehicleTrackingDetails>>() {
                                    }.getType());

                                    if (vehicleTrackingDetailsList != null && vehicleTrackingDetailsList.size() > 800) {
                                        Toast.makeText(context, "Too many vehicles to show on map!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        setVehicleList(vehicleTrackingDetailsList);
                                    }
                                } catch (Exception e) {

                                }
                                progressBar.hide();
                            } else {
                                progressBar.hide();
                                //Toast toast = Toast.makeText(context, "Something went wrong! Please try again", Toast.LENGTH_LONG);
                                //toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                //toast.show();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<Result> call, Throwable t) {
                    progressBar.hide();
                    //Toast.makeText(context, "Connection problem ! Try again later.", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private String transporterIdTemp="0";
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && activity!=null) {
            activity.setTitle("Map View");
            Log.d("MyFragment", "Fragment is visible.");
            //if(!transporterIdTemp.equalsIgnoreCase(transporterId)){
              //  transporterIdTemp = transporterId;
                isVisible = true;
                RequestVO requestVO = new RequestVO();
                try {
                    requestVO = new RequestVO();
                    requestVO.setUserId(client_id);
                    if(!transporterId.equalsIgnoreCase("-1")) {
                        requestVO.setTransporterId(encrypt(transporterId));
                    }
                    getVehicleDetails(requestVO);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            //}
        } else {
            isVisible = false;
            Log.d("MyFragment", "Fragment is not visible.");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}