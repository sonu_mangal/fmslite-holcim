package com.trimble.fmsliteholcim.activities.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.trimble.fmsliteholcim.Constants;
import com.trimble.fmsliteholcim.R;
import com.trimble.fmsliteholcim.activities.custom_view.SearchableSpinner;
import com.trimble.fmsliteholcim.entity.CustomerVO;
import com.trimble.fmsliteholcim.entity.Hub;
import com.trimble.fmsliteholcim.entity.LocationVO;
import com.trimble.fmsliteholcim.entity.TripCreations;
import com.trimble.fmsliteholcim.entity.VehicleNumberVO;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.NameValuePair;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.entity.UrlEncodedFormEntity;
import ch.boye.httpclientandroidlib.client.methods.HttpGet;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.entity.StringEntity;
import ch.boye.httpclientandroidlib.impl.client.DefaultHttpClient;
import ch.boye.httpclientandroidlib.message.BasicNameValuePair;

public class CreateTripFragment extends Fragment {
    private Button tripCreate_btn;
    private ImageButton ibtn_info;
    private TextView customer_tv, vehicle_tv, locationFrom_tv, locationTo_tv, departureTime_tv, tat_tv,departure_tv;
    private com.trimble.fmsliteholcim.activities.custom_view.SearchableSpinner
             vehicle_sp, locationFrom_sp, locationTo_sp ;
    private AutoCompleteTextView customer_sp;
    private DatePicker departure_date;
    private EditText tat_ed, et_departureDate, email_sp ;
    private ProgressDialog progressBar;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String userId,getDate,getTime;
    int flag=0;
    private Context context;
    public CreateTripFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        getActivity().setTitle("Create New Trip");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_create_trip, container, false);
        sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        userId = sharedPreferences.getString(Constants.CLIENT_ID,"");
        tripCreate_btn = (Button) rootView.findViewById(R.id.tripCreate_btn);
        customer_tv = (TextView) rootView.findViewById(R.id.customer_tv);
        departure_tv = (TextView) rootView.findViewById(R.id.departure_tv);
        vehicle_tv = (TextView) rootView.findViewById(R.id.vehicle_tv);
        locationFrom_tv = (TextView) rootView.findViewById(R.id.locationFrom_tv);
        locationTo_tv = (TextView) rootView.findViewById(R.id.locationTo_tv);
        customer_sp = (AutoCompleteTextView) rootView.findViewById(R.id.customer_sp);
        vehicle_sp = (SearchableSpinner) rootView.findViewById(R.id.vehicle_sp);
        locationFrom_sp = (SearchableSpinner) rootView.findViewById(R.id.locationFrom_sp);
        locationTo_sp = (SearchableSpinner) rootView.findViewById(R.id.locationTo_sp);
        tat_ed = (EditText) rootView.findViewById(R.id.tat_ed);
        et_departureDate = (EditText) rootView.findViewById(R.id.et_departureDate);
        ImageView img = (ImageView) rootView.findViewById(R.id.images);
        email_sp = (EditText) rootView.findViewById(R.id.email_sp);
     //   ibtn_info=(ImageButton)rootView.findViewById(R.id.ibtn_info);

        final SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar mcurrentTime = Calendar.getInstance(); int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        final TimePickerDialog mTimePicker; mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute)
            {
                TimePicker timePickerq= timePicker;
                Log.d("Time***", selectedHour + ":" + selectedMinute);
             //   Calendar newTime = Calendar.getInstance();
              //  newTime.set(selectedHour, selectedMinute);
                //getTime=timeFormatter.format(newTime.getTime());
                 getTime = getDate+" "+selectedHour+":"+selectedMinute+":12"+".57";
             //   getTime = selectedHour+":"+selectedMinute+":12"+".57";
              //  Log.d("TIme***",newTime.getTime().toString()+"------"+getTime);
                et_departureDate.setText(getTime==null?"":getTime);
            }
        }, hour, minute, true);
        //Yes 24 hour time

        final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar newCalendar = Calendar.getInstance();
        final DatePickerDialog fromDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                getDate=dateFormatter.format(newDate.getTime());
                //timePickerDialog.show();
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

                //  et_departureDate.setText(dateFormatter.format(newDate.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


       // fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        et_departureDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fromDatePickerDialog.show();
            }
        });

        RelativeLayout activity_device = (RelativeLayout) rootView.findViewById(R.id.layout_createtrip);
        activity_device.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard(v);
                return false;
            }
        });

        tripCreate_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String vehicle="";
                String fromLocation="";
                String toLocation="";
                Log.d("Login UserName count",vehicle_sp.getCount()+"");
                String customerName=customer_sp.getText().toString();
                String email=email_sp.getText().toString();

//                if( vehicle_sp.getCount()>0 ){
//                     vehicle=vehicle_sp.getSelectedItem().toString();
//                }else{
//                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                    builder.setMessage("Please select Vehicle Number.")
//                            .setCancelable(false)
//                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.cancel();
//                                }
//                            });
//                    builder.setTitle("FMS Lite");
//                    builder.show();
//                }
//                if(locationFrom_sp.getCount()>0){
//                     fromLocation=locationFrom_sp.getSelectedItem().toString();
//                }else{
//                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                    builder.setMessage("Please select FROM Location.")
//                            .setCancelable(false)
//                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.cancel();
//                                }
//                            });
//                    builder.setTitle("FMS Lite");
//                    builder.show();
//                } if(locationTo_sp.getCount()>0){
//                     toLocation=locationTo_sp.getSelectedItem().toString();
//                }else{
//                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                    builder.setMessage("Please select To Location.")
//                            .setCancelable(false)
//                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.cancel();
//                                }
//                            });
//                    builder.setTitle("FMS Lite");
//                    builder.show();
//                }
                String deprtDate=et_departureDate.getText().toString();
                if(vehicle_sp.getCount()>0 && locationFrom_sp.getCount()>0 && locationTo_sp.getCount()>0){
                    vehicle=vehicle_sp.getSelectedItem().toString();
                    fromLocation=locationFrom_sp.getSelectedItem().toString();
                    toLocation=locationTo_sp.getSelectedItem().toString();
                    if(customerName.length()>0){
                        if(email.length()>0 && ((email.split("\\@",-1).length - 1)==(email.split("\\$",-1).length - 1 )+1)&& emailValidation(email)){
                            if(vehicle.length()>0) {
                                if(locationFrom_sp.getSelectedItemPosition()!=locationTo_sp.getSelectedItemPosition()){
                                    if(deprtDate.length()>0){
                                        if(tat_ed.getText().toString().length()>0 ){
                                            boolean tat= (tat_ed.getText().toString()).matches(".*[a-zA-Z]+.*");
                                            if(!tat){
                                                TripCreations tripCreations = new TripCreations();
                                                tripCreations.setUserId(userId);
                                                tripCreations.setCustomerName(customerName);
                                                tripCreations.setEmailIds(email);
                                                tripCreations.setSourceLocation(fromLocation);
                                                tripCreations.setDestinationLocation(toLocation);
                                                tripCreations.setVehicleNumber(vehicle);
                                                tripCreations.setDepartureTime(deprtDate);
                                                tripCreations.setPlannedTat(tat_ed.getText().toString());
                                                progressBar = new ProgressDialog(getActivity());
                                                progressBar.setCancelable(false);
                                                progressBar.setMessage("Please Wait...");
                                                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                                progressBar.show();
                                                new CreateNewTrip().execute(tripCreations);
                                            }else{
                                                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                                builder.setMessage("Please Insert Valid TAT number.")
                                                        .setCancelable(false)
                                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.cancel();
                                                            }
                                                        });
                                                builder.setTitle("FMS Lite");
                                                builder.show();
                                            }

                                        }else{
                                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                            builder.setMessage("Please Insert TAT number.")
                                                    .setCancelable(false)
                                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.cancel();
                                                        }
                                                    });
                                            builder.setTitle("FMS Lite");
                                            builder.show();
                                        }
                                    }else{
                                        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                        builder.setMessage("Please Select Departure Date.")
                                                .setCancelable(false)
                                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });
                                        builder.setTitle("FMS Lite");
                                        builder.show();
                                    }
                                }else{
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setMessage("Source and Destination location must be different.")
                                            .setCancelable(false)
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });
                                    builder.setTitle("FMS Lite");
                                    builder.show();
                                }
                            }else{
                                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage("Please Select Vehicle Number")
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                builder.setTitle("FMS Lite");
                                builder.show();
                            }
                        }else{
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage("Please insert valid Email Id.")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            builder.setTitle("FMS Lite");
                            builder.show();
                        }
                    }else{
                        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Please insert Customer Name.")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        builder.setTitle("FMS Lite");
                        builder.show();
                        }
                }else{
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please fill complete form.")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    builder.setTitle("FMS Lite");
                    builder.show();
                }
            }
        });

        customer_sp.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
//                Toast.makeText(context,
//                        parent.getAdapter().getItem(position).toString(),
//                        Toast.LENGTH_SHORT).show();
                CustomerVO customerVO = (CustomerVO)parent.getAdapter().getItem(position);
//                ArrayAdapter<String> dataAdapterEmail = new ArrayAdapter<String>(getActivity(),
//                        android.R.layout.simple_spinner_item, customerVO.getEmailId().get(0).split("\\$"));
//                dataAdapterEmail.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                email_sp.setText(customerVO.getEmailId().get(0));
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();  // SET TITLE ON FRAGMENT
        getActivity().setTitle(R.string.create_trip_title);
        progressBar =new ProgressDialog(getActivity());
        progressBar.setCancelable(false);
        progressBar.setMessage("Please Wait...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.show();
        new HubData().execute(userId);
    }

    public boolean emailValidation(String email){
        StringTokenizer tokenizer=new StringTokenizer(email,"$");
        int flag=1;
        while(tokenizer.hasMoreTokens()){
            if(!(Pattern.matches("\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b",tokenizer.nextToken()))){
                flag=0;
                break;
            }
        }
        if(flag==1)
            return true;
        else
            return false;
    }
    class HubData extends AsyncTask<String, Void, List<Hub>> {
        @Override
        protected List<Hub> doInBackground(String... para) {
            try {
                HttpClient httpClient = new DefaultHttpClient();
                String ip = String.format(Constants.GET_HUB_DATA +para[0]);
                Log.d("Show Data URL",ip);
                HttpGet httpGet = new HttpGet(ip);
                Gson gson = new Gson();
                HttpResponse httpResponse = httpClient.execute(httpGet);
                InputStream inputStream = httpResponse.getEntity().getContent();
                StringBuilder sb = new StringBuilder();
                if (inputStream != null) {
                    BufferedReader rd = new BufferedReader(
                            new InputStreamReader(inputStream));
                    String responseStr = "";
                    if (rd != null) {
                        while ((responseStr = rd.readLine()) != null) {
                            sb.append(responseStr);
                        }
                    }
                }
                List<Hub> hubLists = null;
                try {
                    if (sb != null) {
                        System.out.println(sb.toString());
                        Type listType = new TypeToken<ArrayList<Hub>>() {}.getType();
                        hubLists = gson.fromJson(sb.toString(), listType);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return hubLists;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final List<Hub> result) {
            super.onPostExecute(result);
//            Log.d("Trip ",result.size()+"");
            if (progressBar != null){
                progressBar.dismiss();
            }
            if(result != null && result.size()>0){

                ArrayAdapter<CustomerVO> dataAdapterCustmer = new ArrayAdapter<CustomerVO>(getActivity(),
                        android.R.layout.simple_spinner_item, result.get(0).getCustomers());
                dataAdapterCustmer.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                customer_sp.setAdapter(dataAdapterCustmer);

                ArrayAdapter<VehicleNumberVO> dataAdapterVehicle = new ArrayAdapter<>(getActivity(),
                        android.R.layout.simple_spinner_item, result.get(0).getVehicles());
                dataAdapterVehicle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                vehicle_sp.setAdapter(dataAdapterVehicle);

                ArrayAdapter<LocationVO> dataAdapterFromLoc = new ArrayAdapter<LocationVO>(getActivity(),
                        android.R.layout.simple_spinner_item, result.get(0).getLocations());
                dataAdapterFromLoc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                locationFrom_sp.setAdapter(dataAdapterFromLoc);

                ArrayAdapter<LocationVO> dataAdapterToLoc = new ArrayAdapter<LocationVO>(getActivity(),
                        android.R.layout.simple_spinner_item, result.get(0).getLocations());
                dataAdapterToLoc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                locationTo_sp.setAdapter(dataAdapterToLoc);


            }else {
                //  tv_notrip.setVisibility(View.VISIBLE);
            }

        }
    }


    class CreateNewTrip extends AsyncTask<TripCreations, Void, String> {
        @Override
        protected String doInBackground(TripCreations... params) {
            try{
                TripCreations tripCreations = params[0];
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(Constants.CREATE_TRIP_URL);
                Gson gson = new Gson();
                List<NameValuePair> list=new ArrayList<NameValuePair>();
                list.add(new BasicNameValuePair("userId", userId));
                list.add(new BasicNameValuePair("customerName", tripCreations.getCustomerName()));
                list.add(new BasicNameValuePair("emailIds", tripCreations.getEmailIds()));
                list.add(new BasicNameValuePair("vehicleNumber", tripCreations.getVehicleNumber()));
                list.add(new BasicNameValuePair("sourceLocation", tripCreations.getSourceLocation()));
                list.add(new BasicNameValuePair("destinationLocation", tripCreations.getDestinationLocation()));
                list.add(new BasicNameValuePair("departureTime", tripCreations.getDepartureTime()));
                list.add(new BasicNameValuePair("plannedTat", tripCreations.getPlannedTat()));
                System.out.println(gson.toJson(params[0]));
                //StringEntity se = new StringEntity(gson.toJson(tripCreations));
               // httpPost.setEntity(se);

                httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");
                //HttpResponse httpResponse = httpClient.execute(httpPost);

                httpPost.setEntity(new UrlEncodedFormEntity(list));
                HttpResponse httpResponse = httpClient.execute(httpPost);

                InputStream inputStream = httpResponse.getEntity().getContent();
                StringBuilder sb = new StringBuilder();
                Log.d("Sb Result",sb.toString());
                if (inputStream != null) {
                    BufferedReader rd = new BufferedReader(
                            new InputStreamReader(inputStream));
                    String responseStr = "";
                    if (rd != null) {
                        while ((responseStr = rd.readLine()) != null) {
                            sb.append(responseStr);
                        }
                    }
                }

                //**************
                Log.d("Return Sb",sb.toString());
                return sb.toString();
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String response){
            super.onPostExecute(response);
//            Log.d("Create trip Res",response);
            progressBar.dismiss();
            progressBar = null;
            if(response!=null && Integer.parseInt(response)==1){
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Trip Created Successfully.")
                        .setIcon(R.mipmap.correct_icon)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                            }
                        });
                builder.setTitle("FMS Lite");
                builder.show();
                resetFragment();
            }else if(response!=null && Integer.parseInt(response)==0) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Trip not created. Please try after some time.")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                builder.setTitle("FMS Lite");
                builder.show();
            } else{
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Server not Responding.Please Try after some time.")
                            .setIcon(R.drawable.notification_icon)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    builder.setTitle("FMS Lite");
                    builder.show();
                }
            }
    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void resetFragment(){
        customer_sp.setText("");
        email_sp.setText("");
        et_departureDate.setText("");
        tat_ed.setText("");
    }


}
