package com.trimble.fmsliteholcim.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.view.Gravity;
import android.view.MenuInflater;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.trimble.fmsliteholcim.ApiClient;
import com.trimble.fmsliteholcim.ApiInterface;
import com.trimble.fmsliteholcim.Constants;
import com.trimble.fmsliteholcim.R;
import com.trimble.fmsliteholcim.activities.fragments.CreateTripFragment;
import com.trimble.fmsliteholcim.activities.fragments.DashboardHomeFragment;
import com.trimble.fmsliteholcim.activities.fragments.DisplayTripFragment;
import com.trimble.fmsliteholcim.entity.RequestVO;
import com.trimble.fmsliteholcim.entity.Result;
import com.trimble.fmsliteholcim.entity.Vehicle;
import com.trimble.fmsliteholcim.entity.VehicleTrackingDetails;
import com.trimble.fmsliteholcim.utils.EncryptData;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class DashboardDrawerActivity extends AnimatedAppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private SharedPreferences sharedPreferences;
    private String userName, userId;
    private Boolean createTrip, displayTrip;
    private NavigationView navigationView;
    private ProgressDialog progressBar;
    private View fragment;
    private Context context;
   // private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        /*mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);*/

        context = this;
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        fragment = (View)findViewById(R.id.fragment);

        sharedPreferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        userName = sharedPreferences.getString(Constants.USERNAME, "");
        userId = sharedPreferences.getString(Constants.CLIENT_ID, "");

        createTrip = sharedPreferences.getBoolean(Constants.CREATE_TRIP_FLAG, false);
        displayTrip = sharedPreferences.getBoolean(Constants.DISPLAY_TRIP_FLAG, false);

        String versionName = sharedPreferences.getString(Constants.VERSION_NAME, "");

        //setTitle("FMS Lite "+versionName);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerLayout = navigationView.getHeaderView(0);
        TextView tv_username = (TextView) headerLayout.findViewById(R.id.tv_username);
        TextView tv_fms = (TextView) headerLayout.findViewById(R.id.tv_fms);
        tv_fms.setText(getString(R.string.app_name)+" v" + versionName);

        tv_username.setText(userName);

        fragment.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard(view);
                return false;
            }
        });
        setFragment(0);
        hideMenuItem();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            final AlertDialog.Builder builder = new AlertDialog.Builder(DashboardDrawerActivity.this);
            builder.setTitle("Confirm !");
            builder.setMessage("Are you sure to exit?").setCancelable(false);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    finish();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.create();
            builder.show();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        if(searchView!=null){
            searchView.setQuery("", false);
            searchView.clearFocus();
            if(searchItem!=null) {
                searchItem.collapseActionView();
            }
            hideKeyboard(searchView);
        }
    }

    SearchView searchView;
    MenuItem searchItem;
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search, menu);
        searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView ) searchItem.getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setIconifiedByDefault(false);

            SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
                public boolean onQueryTextChange(String query) {
                    return true;
                }

                public boolean onQueryTextSubmit(String query) {
                    searchView.clearFocus();
                    if (query.length() >= 4) {
                        getVehicleList(query);
                    }else{
                        Toast.makeText(context,"Please enter at least 4 characters",Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        return super.onCreateOptionsMenu(menu);
    }

    private void getVehicleDetails(RequestVO requestVO) {

        if(searchView!=null){
            searchView.setQuery("", false);
            searchView.clearFocus();
            if(searchItem!=null) {
                searchItem.collapseActionView();
            }
            hideKeyboard(searchView);
        }

        progressBar.setMessage("Loading...");
        progressBar.show();

        ApiInterface apiService = ApiClient.getFMSServices(this).create(ApiInterface.class);

        Call<Result> call = apiService.getVehicleTrackingDetails(requestVO);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, retrofit2.Response<Result> response) {
                //int statusCode = response.code();
                progressBar.dismiss();
                Result result = response.body();
                if (result != null && result.getStatus().equalsIgnoreCase("success")) {
                    if (result.getData() != null && result.getData() != "") {
                        Gson gson = new Gson();
                        JsonArray jsonArray = gson.toJsonTree(result.getData()).getAsJsonArray();
                        List<VehicleTrackingDetails> vehicleTrackingDetailsList = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<VehicleTrackingDetails>>() {
                        }.getType());

                        if (vehicleTrackingDetailsList != null && vehicleTrackingDetailsList.size() > 0) {
                            final VehicleTrackingDetails vehicleTrackingDetails = vehicleTrackingDetailsList.get(0);

                            final Dialog dialog = new Dialog(DashboardDrawerActivity.this);
                            dialog.setContentView(R.layout.custom_dialog_vehicle_details);
                            //dialog.setTitle("Title...");

                            TextView tv_vehicle_number = (TextView) dialog.findViewById(R.id.tv_vehicle_number);
                            TextView tv_last_reported = (TextView) dialog.findViewById(R.id.tv_last_reported);

                            tv_vehicle_number.setText(vehicleTrackingDetails.getVehicleNumber());
                            tv_last_reported.setText("Vehicle Status : " + (vehicleTrackingDetails.getIsNRD().equalsIgnoreCase("1") ? " NRD " : vehicleTrackingDetails.getIsStopped().equalsIgnoreCase("1") ?
                                    " Stopped \nStoppage Duration : " + vehicleTrackingDetails.getStoppageDuration()
                                            + " \nStoppage DateTime : " + vehicleTrackingDetails.getStoppageDateTime() :
                                    vehicleTrackingDetails.getIsRunning().equalsIgnoreCase("1") ? "Running" : "NA")
                                    + "\nSpeed : " + vehicleTrackingDetails.getSpeed() + " Kms"
                                    + "\nLast Reported : " + vehicleTrackingDetails.getLastLocationDatetime()
                                    + "\nDriver Name : " + vehicleTrackingDetails.getDriverName()
                                    + "\nDriver Mobile : " + vehicleTrackingDetails.getDriverMobile()
                                    + "\nLast Location : " + vehicleTrackingDetails.getLastLocation()
                                    + "\nTransporter Name : " + vehicleTrackingDetails.getTransporterName());

                            ImageButton ib_location = (ImageButton) dialog.findViewById(R.id.ib_location);
                            // if button is clicked, close the custom dialog
                            ib_location.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    try {
                                        String location = "geo:0,0?q=" + vehicleTrackingDetails.getLatitude() + "," + vehicleTrackingDetails.getLongitude() + "(" + vehicleTrackingDetails.getLastLocation() + ")";
                                        if (vehicleTrackingDetails.getLastLocation().equalsIgnoreCase(null) || vehicleTrackingDetails.getLastLocation().equalsIgnoreCase("")) {
                                            location = "http://maps.google.com/maps?q=" + vehicleTrackingDetails.getLatitude() + "," + vehicleTrackingDetails.getLongitude();
                                        }
                                        Uri gmmIntentUri = Uri.parse(location);
                                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                                        mapIntent.setPackage("com.google.android.apps.maps");
                                        startActivity(mapIntent);

                                    } catch (Exception e) {
                                        Toast.makeText(DashboardDrawerActivity.this, "Google map in your phone is having issue to open", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                            Button btn_close = (Button)dialog.findViewById(R.id.btn_close);
                            btn_close.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                }
                            });

                            dialog.show();

/*
                            AlertDialog.Builder builderInner = new AlertDialog.Builder(DashboardDrawerActivity.this);
                            builderInner.setMessage("Vehicle Number : " + vehicleTrackingDetails.getVehicleNumber()
                                    + "\nVehicle Status : " + (vehicleTrackingDetails.getIsNRD().equalsIgnoreCase("1") ? " NRD " : vehicleTrackingDetails.getIsStopped().equalsIgnoreCase("1") ?
                                    " Stopped \nStoppage Time : " + vehicleTrackingDetails.getStoppageDateTime()
                                            + " \nStoppage Duration : " + vehicleTrackingDetails.getStoppageDuration()
                                            + " \nStoppage DateTime : " + vehicleTrackingDetails.getStoppageDateTime() :
                                    vehicleTrackingDetails.getIsRunning().equalsIgnoreCase("1") ? "Running" : "NA")
                                    + "\nSpeed : " + vehicleTrackingDetails.getSpeed() + " Kms"
                                    + "\nLast Reported : " + vehicleTrackingDetails.getLastLocationDatetime()
                                    + "\nDriver Name : " + vehicleTrackingDetails.getDriverName()
                                    + "\nDriver Mobile : " + vehicleTrackingDetails.getDriverMobile()
                                    + "\nLast Location : " + vehicleTrackingDetails.getLastLocation()
                                    + "\nTransporter Name : " + vehicleTrackingDetails.getTransporterName());
                            //builderInner.setTitle("Search Result");
                            builderInner.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builderInner.setPositiveButton("View on Map", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    try {
                                        String location = "geo:0,0?q=" + vehicleTrackingDetails.getLatitude() + "," + vehicleTrackingDetails.getLongitude() + "(" + vehicleTrackingDetails.getLastLocation() + ")";
                                        if (vehicleTrackingDetails.getLastLocation().equalsIgnoreCase(null) || vehicleTrackingDetails.getLastLocation().equalsIgnoreCase("")) {
                                            location = "http://maps.google.com/maps?q=" + vehicleTrackingDetails.getLatitude() + "," + vehicleTrackingDetails.getLongitude();
                                        }
                                        Uri gmmIntentUri = Uri.parse(location);
                                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                                        mapIntent.setPackage("com.google.android.apps.maps");
                                        startActivity(mapIntent);

                                    } catch (Exception e) {
                                        Toast.makeText(DashboardDrawerActivity.this, "Google map in your phone is having issue to open", Toast.LENGTH_SHORT).show();
                                    }

                                }
                            });
                            builderInner.show();
*/
                        }
                    }
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Something went wrong! Please re-open the app",
                            Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                progressBar.dismiss();
                Toast.makeText(DashboardDrawerActivity.this, "Connection problem ! Try again later.", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getVehicleList(final String vehicleNumberString) {

        progressBar.setMessage("Loading...");
        progressBar.show();

        RequestVO requestVO = new RequestVO();
        try {
            String ecryptedUserId = EncryptData.encrypt(userId);
            String ecryptedVehicleNumber = EncryptData.encrypt('%' + vehicleNumberString + '%');

            requestVO.setUserId(ecryptedUserId);
            requestVO.setVehicleNumber(ecryptedVehicleNumber);
            requestVO.setIsEncrypted(1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ApiInterface apiService = ApiClient.getFMSServices(this).create(ApiInterface.class);

        Call<Result> call = apiService.getVehicles(requestVO);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, retrofit2.Response<Result> response) {
                //int statusCode = response.code();
                progressBar.dismiss();
                Result result = response.body();
                if (result != null && result.getStatus().equalsIgnoreCase("success")) {
                    if (result.getData() != null && result.getData() != "") {
                        Gson gson = new Gson();
                        JsonArray jsonArray = gson.toJsonTree(result.getData()).getAsJsonArray();
                        List<Vehicle> vehicleList = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<Vehicle>>() {
                        }.getType());
                        if (vehicleList != null && vehicleList.size() > 0) {

                            AlertDialog.Builder builderSingle = new AlertDialog.Builder(DashboardDrawerActivity.this);
                            builderSingle.setTitle("Vehicle Search Result");

                            final ArrayAdapter<Vehicle> arrayAdapter = new ArrayAdapter<Vehicle>(DashboardDrawerActivity.this, android.R.layout.select_dialog_singlechoice);
                            arrayAdapter.addAll(vehicleList);

                            builderSingle.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Vehicle vehicle = arrayAdapter.getItem(which);
                                    RequestVO requestVO = new RequestVO();
                                    try {
                                        String ecryptedUserId = EncryptData.encrypt(userId);
                                        String ecryptedVehicleId = EncryptData.encrypt(vehicle.getVehicleId());

                                        requestVO.setUserId(ecryptedUserId);
                                        requestVO.setVehicleId(ecryptedVehicleId);
                                        requestVO.setIsEncrypted(1);
                                        getVehicleDetails(requestVO);

                                        hideKeyboard(navigationView);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            builderSingle.show();

                        } else {
                            Toast.makeText(getApplicationContext(), "No result found for Vehicle No. " + vehicleNumberString, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "No result found for Vehicle No. " + vehicleNumberString, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Something went wrong! Please re-open the app",
                            Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                }

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                progressBar.dismiss();
                Toast.makeText(DashboardDrawerActivity.this, "Connection problem ! Try again later.", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_dashboard) {
            setFragment(0);
        //} else if (id == R.id.nav_mapview) {
            //setFragment(5);
        }else if (id == R.id.nav_create_new_trip) {
            setFragment(1);
        } else if (id == R.id.nav_show_trip_list) {
            setFragment(2);
        } else if (id == R.id.nav_faq) {
            setFragment(3);
        } else if (id == R.id.nav_logout) {
            setFragment(4);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /*public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0: DashboardFragment tab1 = new DashboardFragment();
                    return tab1;
                case 1: DisplayTripFragment tab2 = new DisplayTripFragment();
                    return tab2;
                default: return  null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Dashboard";
                case 1:
                    return "Mapview";
            }
            return null;
        }

    }*/

    public void hideMenuItem() {
        Menu nav_Menu = navigationView.getMenu();

        nav_Menu.findItem(R.id.nav_create_new_trip).setVisible(createTrip);
        nav_Menu.findItem(R.id.nav_show_trip_list).setVisible(displayTrip);

    }

    public void setFragment(int position) {
        FragmentManager fragmentManager;
        FragmentTransaction fragmentTransaction;
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        switch (position) {
            case 0:
                DashboardHomeFragment fragmentHome = new DashboardHomeFragment();
                fragmentTransaction.replace(R.id.fragment, fragmentHome);
                fragmentTransaction.commit();
                break;
            case 1:
                CreateTripFragment tripFragment = new CreateTripFragment();
                fragmentTransaction.replace(R.id.fragment, tripFragment);
                fragmentTransaction.commit();
                break;
            case 2:
                DisplayTripFragment displayTripFragment = new DisplayTripFragment();
                fragmentTransaction.replace(R.id.fragment, displayTripFragment);
                fragmentTransaction.commit();
                break;
            case 3:
                FAQActivity faqActivity = new FAQActivity();
                fragmentTransaction.replace(R.id.fragment, faqActivity);
                fragmentTransaction.commit();
                break;
            case 4:
                final AlertDialog.Builder builder = new AlertDialog.Builder(DashboardDrawerActivity.this);
                builder.setTitle("Confirm !");
                builder.setMessage("Are you sure to exit?").setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        startActivity(new Intent(DashboardDrawerActivity.this, LoginActivity.class));
                        finish();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.create();
                builder.show();
                break;
            case 5:
//                MapViewFragment fragmentMap = MapViewFragment.newInstance(1,"Map View");
//                fragmentMap.transporterId = transporterId
//                fragmentTransaction.replace(R.id.fragment, fragmentMap);
//                fragmentTransaction.commit();
                break;
        }
    }

    public void showMap(Double lat, Double lang, String locationName) {
        try {
           /* Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?q=" + lat + "," + lang));
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // Only if initiating from a Broadcast Receiver
            String mapsPackageName = "com.google.android.apps.maps";
            i.setClassName(mapsPackageName, "com.google.android.maps.MapsActivity");
            i.setPackage(mapsPackageName);
            startActivity(i);*/

            String location = "geo:0,0?q=" + lat + "," + lang + "(" + locationName + ")";

            if (locationName.equalsIgnoreCase("Location is unknown")) {
                location = "http://maps.google.com/maps?q=" + lat + "," + lang;
            }
            //Uri gmmIntentUri = Uri.parse("geo:"+lat+","+lang);
            Uri gmmIntentUri = Uri.parse(location);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);

        } catch (Exception e) {
            Toast.makeText(this, "Google map in your phone is having issue to open", Toast.LENGTH_SHORT).show();
        }
    }


    String phoneNumber;
    private void canCall() {
        if (phoneNumber != null && !phoneNumber.isEmpty()) {
            Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
            try {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(in);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(context, "Application Not found to make call", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void callDriver(String mobileNo) {
        phoneNumber = mobileNo;
        makeCall();
    }

    private void makeCall() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_CALL);
        } else {
            canCall();
        }
    }

    int PERMISSION_CALL = 123;
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == PERMISSION_CALL) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                canCall();
                // Snackbar.make(view,"Permission Granted, Now you can access location data.",Snackbar.LENGTH_LONG).show();
            } else {
                // Snackbar.make(view,"Permission Denied, You cannot access location data.",Snackbar.LENGTH_LONG).show();
            }
        }
    }

}
