package com.trimble.fmsliteholcim.activities.custom_view.animation;

/**
 * Created by sbhosal on 10/18/17.
 */

        import android.view.animation.Animation;
        import android.view.animation.Transformation;

        import com.trimble.fmsliteholcim.activities.custom_view.PieView;


public class PieAngleAnimation extends Animation {

    private PieView arcView;
    private float oldAngle;

    public PieAngleAnimation(PieView arcView) {
        this.oldAngle = arcView.getPieAngle();
        this.arcView = arcView;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation transformation) {
        float angle = oldAngle * interpolatedTime;
        arcView.setPieAngle(angle);
        arcView.requestLayout();
    }
}