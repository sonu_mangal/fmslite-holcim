package com.trimble.fmsliteholcim.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.trimble.fmsliteholcim.ApiClient;
import com.trimble.fmsliteholcim.ApiInterface;
import com.trimble.fmsliteholcim.Constants;
import com.trimble.fmsliteholcim.R;
import com.trimble.fmsliteholcim.adapters.DriverListAdaptor;
import com.trimble.fmsliteholcim.entity.RequestVO;
import com.trimble.fmsliteholcim.entity.Result;
import com.trimble.fmsliteholcim.entity.VehicleTrackingDetails;
import com.trimble.fmsliteholcim.utils.EncryptData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by swapnil on 27/12/16.
 */
public class DriverListActivity extends AnimatedAppCompatActivity {

    private DriverListAdaptor productEnquiryListAdapter;
    private ListView listView;
    private TextView tv_msg;
    private RelativeLayout layout_driver_list;
    private int PERMISSION_CALL = 210;
    private String phoneNumber = "";
    private ArrayList<Map<String, String>> list;
    private ProgressDialog progressBar;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        sharedPreferences = getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        userId = sharedPreferences.getString(Constants.CLIENT_ID, "");

        String transporterIdEncrypted = getIntent().getStringExtra(Constants.TRANSPORTER_ID);

        setTitle(getIntent().getStringExtra(Constants.TYPE) + " Vehicles");

        productEnquiryListAdapter = new DriverListAdaptor(DriverListActivity.this, new ArrayList<VehicleTrackingDetails>());

        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(productEnquiryListAdapter);

        tv_msg = (TextView) findViewById(R.id.tv_msg);
        layout_driver_list = (RelativeLayout)findViewById(R.id.layout_driver_list);

        listView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard(view);
                return false;
            }
        });


        final EditText et_search = (EditText) findViewById(R.id.et_search);
        if (et_search != null) {
            et_search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (productEnquiryListAdapter != null) {
                        productEnquiryListAdapter.filter(s.toString());
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }

        RequestVO requestVO = new RequestVO();
        try {

            requestVO.setUserId(EncryptData.encrypt(userId));
            if (transporterIdEncrypted != null && !transporterIdEncrypted.equalsIgnoreCase("0")) {
                Log.d("ID", transporterIdEncrypted);
                requestVO.setTransporterId(transporterIdEncrypted);
            }
            switch (getIntent().getStringExtra(Constants.TYPE)) {
                case "NRD":
                    requestVO.setIsNRD(EncryptData.encrypt("1"));
                    break;
                case "Moving":
                    requestVO.setIsRunning(EncryptData.encrypt("1"));
                    break;
                case "Idle":
                    requestVO.setIsStopped(EncryptData.encrypt("1"));
                    break;
                default:
                    break;
            }
            getVehicleDetails(requestVO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getVehicleDetails(RequestVO requestVO) {
        requestVO.setIsEncrypted(1);

        progressBar.setMessage("Loading...");
        progressBar.show();

        ApiInterface apiService = ApiClient.getFMSServices(this).create(ApiInterface.class);

        Call<Result> call = apiService.getVehicleTrackingDetails(requestVO);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, retrofit2.Response<Result> response) {
                //int statusCode = response.code();
                Result result = response.body();
                System.out.println("Vehicle List : " + result.getData());
                if (result != null && result.getStatus().equalsIgnoreCase("success")) {
                    if (result.getData() != null && result.getData() != "") {
                        Gson gson = new Gson();
                        JsonArray jsonArray = gson.toJsonTree(result.getData()).getAsJsonArray();
                        final List<VehicleTrackingDetails> vehicleTrackingDetailsList = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<VehicleTrackingDetails>>() {
                        }.getType());
                        setTitle(getIntent().getStringExtra(Constants.TYPE) + " Vehicles | " + getIntent().getIntExtra(Constants.SELECTED_COUNT, 0));
                        productEnquiryListAdapter.setDistributorList(vehicleTrackingDetailsList);
                        progressBar.dismiss();
                    } else {
                        progressBar.dismiss();
                        Toast toast = Toast.makeText(getApplicationContext(), "Something went wrong! Please try again", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        toast.show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                progressBar.dismiss();
                Toast.makeText(DriverListActivity.this, "Connection problem ! Try again later.", Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (progressBar != null) {
            progressBar.dismiss();
        }
        hideKeyboard(tv_msg);
    }

    public void showMap(Double lat, Double lang, String locationName) {
        try {
           /* Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?q=" + lat + "," + lang));
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // Only if initiating from a Broadcast Receiver
            String mapsPackageName = "com.google.android.apps.maps";
            i.setClassName(mapsPackageName, "com.google.android.maps.MapsActivity");
            i.setPackage(mapsPackageName);
            startActivity(i);*/

            String location = "geo:0,0?q=" + lat + "," + lang + "(" + locationName + ")";

            if (locationName.equalsIgnoreCase("Location is unknown")) {
                location = "http://maps.google.com/maps?q=" + lat + "," + lang;
            }
            //Uri gmmIntentUri = Uri.parse("geo:"+lat+","+lang);
            Uri gmmIntentUri = Uri.parse(location);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);

        } catch (Exception e) {
            Toast.makeText(this, "Google map in your phone is having issue to open", Toast.LENGTH_SHORT).show();
        }
    }


    private void canCall() {
        if (phoneNumber != null && !phoneNumber.isEmpty()) {
            Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
            try {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(in);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(DriverListActivity.this, "Application Not found to make call", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void callDriver(String mobileNo) {
        phoneNumber = mobileNo;
        makeCall();
    }

    private void makeCall() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_CALL);
        } else {
            canCall();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == PERMISSION_CALL) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                canCall();
                // Snackbar.make(view,"Permission Granted, Now you can access location data.",Snackbar.LENGTH_LONG).show();
            } else {
                // Snackbar.make(view,"Permission Denied, You cannot access location data.",Snackbar.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}