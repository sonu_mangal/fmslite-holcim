package com.trimble.fmsliteholcim.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.trimble.fmsliteholcim.ApiClient;
import com.trimble.fmsliteholcim.ApiInterface;
import com.trimble.fmsliteholcim.Constants;
import com.trimble.fmsliteholcim.R;
import com.trimble.fmsliteholcim.activities.custom_view.NoDefaultSpinner;
import com.trimble.fmsliteholcim.entity.Result;
import com.trimble.fmsliteholcim.entity.VehicleTrackingDetails;
import com.trimble.fmsliteholcim.entity.VehicleTrailDetails;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

import static com.trimble.fmsliteholcim.utils.EncryptData.encrypt;
public class TrailRouteActivity extends AnimatedAppCompatActivity implements OnMapReadyCallback {
    MapView mMapView;
    private GoogleMap googleMap;
    private ProgressDialog progressBar;
    LatLng sydney;
    //String[] trialTimeSlot = { "Last 6 hrs", "Last 12 hrs", "Last 24 hrs", "Other" };
    private String selectedFromDate,selectedFromTime,selectedToDate,selectedToTime;
    private boolean dateFlag;
    private DatePickerDialog fromDatePickerDialog;
    private VehicleTrailDetails vehicleTrailDetails;
    private SimpleDateFormat sdf;
    private MarkerOptions markerOptionsVehicle;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trail_route);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        context = this;
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);

        final VehicleTrackingDetails vehicleTrackingDetails = getIntent().getParcelableExtra(Constants.VEHICLE_DETAILS);
        NoDefaultSpinner spin = (NoDefaultSpinner) findViewById(R.id.spinner_trail_time);

        //  ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,trialTimeSlot);
//        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spin.setAdapter(aa);

        setTitle("Vehicle Trail : "+vehicleTrackingDetails.getVehicleNumber());

        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // vehicleId

        vehicleTrailDetails = new VehicleTrailDetails();

        System.out.println(new Gson().toJson(vehicleTrackingDetails));

        try {
            vehicleTrailDetails.setVehicleId(encrypt(vehicleTrackingDetails.getVehicleId()));
            vehicleTrailDetails.setIsEncrypted(1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*final SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:MM:SS");
        Calendar newCalendar1 = Calendar.getInstance();
        final TimePickerDialog timePickerDialog = new TimePickerDialog(this,new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                *//*Calendar newTime = Calendar.getInstance();
                newTime.set(hourOfDay, minute);*//*
                if(dateFlag){
                    selectedFromTime=(hourOfDay<10? "0"+hourOfDay : hourOfDay) + ":" + (minute<10? "0"+minute : minute) + ":00";
                    adtv_from_date.setText(selectedFromDate+" "+selectedFromTime);
                }else {
                    selectedToTime = (hourOfDay<10? "0"+hourOfDay : hourOfDay) + ":" + (minute<10? "0"+minute : minute) + ":00";
                    adtv_to_date.setText(selectedToDate+" "+selectedToTime);
                }
            }
        }, newCalendar1.get(Calendar.HOUR), newCalendar1.get(Calendar.MINUTE), false);*/

        final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                if(dateFlag)selectedFromDate=dateFormatter.format(newDate.getTime());
                else selectedToDate= dateFormatter.format(newDate.getTime());
                adtv_from_date.setText(selectedFromDate);
                /*timePickerDialog.setTitle("Select Time");
                timePickerDialog.show();*/

            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        fromDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);

        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    vehicleTrailDetails.setToDate(encrypt(sdf.format(new Date())));
                    switch (position){
                        case 0:
                            vehicleTrailDetails.setFromDate(encrypt(sdf.format(new Date(System.currentTimeMillis() - 3600 * 1000 * 6))));
                            getVehicleTrail(vehicleTrailDetails);    break;
                        case 1:  vehicleTrailDetails.setFromDate(encrypt(sdf.format(new Date(System.currentTimeMillis() - 3600 * 1000 * 12))));
                            getVehicleTrail(vehicleTrailDetails); break;
                        case 2:  vehicleTrailDetails.setFromDate(encrypt(sdf.format(new Date(System.currentTimeMillis() - 3600 * 1000 * 24))));
                            getVehicleTrail(vehicleTrailDetails);  break;
                        case 3:  adtv_from_date.setText("");
                                // adtv_to_date.setText("");
                                 dialog.show();
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*mMapView = (MapView) findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); */// needed to get the map to display immediately
        mMapView = (MapView) findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        //mMapView.getMapAsync(this);

        try {
            MapsInitializer.initialize(this.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }


        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                googleMap.getUiSettings().setZoomControlsEnabled(true);

                sydney = new LatLng(vehicleTrackingDetails.getLatitude(), vehicleTrackingDetails.getLongitude());
                markerOptionsVehicle = new MarkerOptions().position(sydney);
                markerOptionsVehicle.title(vehicleTrackingDetails.getVehicleNumber()+" - "+vehicleTrackingDetails.getSpeed()+"Km/h").snippet(vehicleTrackingDetails.getLastLocation());

                BitmapDrawable bitmapdraw = (BitmapDrawable)getResources().getDrawable(R.drawable.veh_moving);

                if(vehicleTrackingDetails.getIsNRD().equalsIgnoreCase("1")){
                    bitmapdraw = (BitmapDrawable)getResources().getDrawable(R.drawable.veh_stopped);
                }else if(vehicleTrackingDetails.getIsRunning().equalsIgnoreCase("1")){
                    bitmapdraw = (BitmapDrawable)getResources().getDrawable(R.drawable.veh_moving);
                }else if(vehicleTrackingDetails.getIsStopped().equalsIgnoreCase("1")){
                    bitmapdraw = (BitmapDrawable)getResources().getDrawable(R.drawable.veh_nrd);
                }

                Bitmap b=bitmapdraw.getBitmap();
                Bitmap smallMarker = Bitmap.createScaledBitmap(b, 80, 80, false);
                markerOptionsVehicle.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));

                //markerOptionsVehicle.icon(BitmapDescriptorFactory.fromResource(R.drawable.arrow_small));

                googleMap.addMarker(markerOptionsVehicle);
                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                //googleMap.animateCamera(MarkerAnimation.animateMarkerToGB());
                //new MarkerAnimation.animateMarkerToICS(marker, toLocation, new LatLngInterpolator.Spherical());

            }
        });

        createDialogForDate();
    }
//
//    GoogleMap.CancelableCallback simpleAnimationCancelableCallback =
//            new GoogleMap.CancelableCallback(){
//
//                @Override
//                public void onCancel() {
//                }
//
//                @Override
//                public void onFinish() {
//
//                        if(++currentPt < markers.size()){
//
//                        CameraPosition cameraPosition =
//                                new CameraPosition.Builder()
//                                        .target(targetLatLng)
//                                        .tilt(currentPt<markers.size()-1 ? 90 : 0)
//                                        //.bearing((float)heading)
//                                        .zoom(googleMap.getCameraPosition().zoom)
//                                        .build();
//
//
//                        googleMap.animateCamera(
//                                CameraUpdateFactory.newCameraPosition(cameraPosition),
//                                3000,
//                                simpleAnimationCancelableCallback);
//
//                        highLightMarker(currentPt);
//
//                    }
//                }
//            };
    public void getVehicleTrail(VehicleTrailDetails requestVO){
        requestVO.setIsEncrypted(1);
        progressBar.setMessage("Loading...");
        progressBar.show();

        if(googleMap!=null) {
            googleMap.clear();
        }

        System.out.println(new Gson().toJson(requestVO));

        ApiInterface apiService = ApiClient.getFMSServices(this).create(ApiInterface.class);
        Call<Result> call = apiService.getVehicleTrailDetails(requestVO);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, retrofit2.Response<Result> response) {
                //int statusCode = response.code();
                Result result = response.body();
                //   System.out.println("Vehicle trail : "+result.getData());
                if (result != null && result.getStatus().equalsIgnoreCase("success")) {
                    if (result.getData() != null && result.getData() != "") {
                        Gson gson = new Gson();
                        JsonArray jsonArray= gson.toJsonTree(result.getData()).getAsJsonArray();
                        final List<VehicleTrailDetails> vehicleTrackingDetailsList = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<VehicleTrailDetails>>(){}.getType());
                        System.out.println(vehicleTrackingDetailsList.size()+"----------");
                        if(vehicleTrackingDetailsList!=null&&vehicleTrackingDetailsList.size()>0){
                            setMarkerOnMap(vehicleTrackingDetailsList);
                        }else{
                            Toast toast = Toast.makeText(getApplicationContext(),"No trail available for this duration!",Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                            toast.show();
                        }
                    } else {
                        Toast toast = Toast.makeText(getApplicationContext(),"Something went wrong! Please try again",Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        toast.show();
                    }
                }
                progressBar.dismiss();
            }
            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                progressBar.dismiss();
                Toast.makeText(TrailRouteActivity.this, "Connection problem ! Try again later.", Toast.LENGTH_LONG).show();
            }
        });
    }

    private EditText adtv_from_date; //,adtv_to_date;
    private Dialog dialog;
    public void createDialogForDate(){
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom_trail_route_date);
        adtv_from_date = (EditText)dialog.findViewById(R.id.adtv_from_date);
        //adtv_to_date = (EditText)dialog.findViewById(R.id.adtv_to_date);
        Button btn_trail_date = (Button)dialog.findViewById(R.id.btn_trail_date);
        adtv_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateFlag = true;
                fromDatePickerDialog.show();
            }
        });
       /* adtv_to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateFlag = false;
                fromDatePickerDialog.show();
            }
        });*/
        btn_trail_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(!adtv_from_date.getText().toString().isEmpty()) {
                        System.out.println(selectedFromDate + " " + "00:00:00");
                        System.out.println(selectedToDate + " " + "23:59:59");

                        vehicleTrailDetails.setFromDate(encrypt(selectedFromDate + " " + "00:00:00"));
                        vehicleTrailDetails.setToDate(encrypt(selectedFromDate + " " + "23:59:59"));
                        getVehicleTrail(vehicleTrailDetails);
                        dialog.cancel();
                    }else{
                        Toast.makeText(context,"Please set duration correctly",Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    dialog.cancel();
                    e.printStackTrace();
                }
            }
        });
    }

    public void setMarkerOnMap(List<VehicleTrailDetails> vehicleTrackingDetailsList){
        if(googleMap!=null) {
            googleMap.clear();
            googleMap.addMarker(markerOptionsVehicle);
            final List<VehicleTrailDetails> temp = vehicleTrackingDetailsList;

            BitmapDescriptor bitmap = BitmapDescriptorFactory.fromResource(R.drawable.arrow_small);
            PolylineOptions rectOptions = new PolylineOptions();
            for (int i = 1; i < temp.size(); i++) {
                LatLng latLng = new LatLng(temp.get(i).getLatitude(), temp.get(i).getLongitude());
                MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(temp.get(i).getVehicleNumber()).snippet(temp.get(i).getLocation() + "\n" + temp.get(i).getSpeed() + "Kmph");
                markerOptions.icon(bitmap);

                if(i+1<temp.size()) {
                    Location location = new Location("");
                    Location location1 = new Location("");

                    LatLng latLng1 = new LatLng(temp.get(i+1).getLatitude(), temp.get(i+1).getLongitude());

                    location.setLatitude(latLng.latitude);
                    location.setLongitude(latLng.longitude);

                    location1.setLatitude(latLng1.latitude);
                    location1.setLongitude(latLng1.longitude);

                    float bearing = location.bearingTo(location1) ;

                    markerOptions.anchor(0.5f, 0.5f)
                            .rotation(bearing)
                            .flat(true);
                }

                googleMap.addMarker(markerOptions);

                rectOptions.add(latLng);
                rectOptions.width(2);
                googleMap.addPolyline(rectOptions);
            }

            googleMap.addMarker(markerOptionsVehicle);


            int midPoint = vehicleTrackingDetailsList.size()/2;
            LatLng latLng1 = new LatLng(vehicleTrackingDetailsList.get(midPoint).getLatitude(), vehicleTrackingDetailsList.get(midPoint).getLongitude());
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng1).zoom(10).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            // Get back the mutable Polyline

            //int midPoint = temp.size()/2;
            //sydney = new LatLng(temp.get(midPoint).getLatitude(), temp.get(midPoint).getLongitude());
            //CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(8).build();
            //googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
