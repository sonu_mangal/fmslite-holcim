package com.trimble.fmsliteholcim.activities.custom_view;

/**
 * Created by sbhosal on 2/5/18.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.trimble.fmsliteholcim.R;

import com.cardiomood.android.controls.gauge.SpeedometerGauge;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.trimble.fmsliteholcim.Constants;
import com.trimble.fmsliteholcim.activities.TrailRouteActivity;
import com.trimble.fmsliteholcim.entity.VehicleTrackingDetails;

public class CustomInfoWindowGoogleMap implements GoogleMap.InfoWindowAdapter {

    private Activity context;
    //private OnInfoWindowElemTouchListener infoButtonListener;

    public CustomInfoWindowGoogleMap(Activity ctx){
        context = ctx;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View convertView = ((Activity)context).getLayoutInflater()
                .inflate(R.layout.list_item_driver, null);
        Typeface tf = Typeface.createFromAsset(context.getAssets(), Constants.LAST_REPORTING_TIME);
        final VehicleTrackingDetails vehicleTrackingDetails = (VehicleTrackingDetails)marker.getTag();

        if(vehicleTrackingDetails!=null) {
            TextView tv_vehicle_number = (TextView) convertView.findViewById(R.id.tv_vehicle_number);
            TextView tv_driver_name = (TextView) convertView.findViewById(R.id.tv_driver_name);
            TextView tv_vehicle_speed = (TextView) convertView.findViewById(R.id.tv_vehicle_speed);
            TextView tv_digital_time = (TextView) convertView.findViewById(R.id.tv_digital_time);
            TextView tv_stoppage_time = (TextView) convertView.findViewById(R.id.tv_stoppage_time);
            ImageButton ib_share = (ImageButton) convertView.findViewById(R.id.ib_share);
            TextView tv_lastlocation_date = (TextView) convertView.findViewById(R.id.tv_lastlocation_date);
            TextView tv_lastlocation_city = (TextView) convertView.findViewById(R.id.tv_lastlocation_city);
            SpeedometerGauge speedometer = (SpeedometerGauge) convertView.findViewById(R.id.speedometer);
            ImageView img_vehicle_type = (ImageView) convertView.findViewById(R.id.img_vehicle_type);
            ImageView img_battery_type = (ImageView) convertView.findViewById(R.id.img_battery_type);
            speedometer.setLabelConverter(new SpeedometerGauge.LabelConverter() {
                @Override
                public String getLabelFor(double progress, double maxProgress) {
                    return String.valueOf((int) Math.round(1));
                }
            });

            ib_share.setVisibility(View.GONE);
            // configure value range and ticks
            speedometer.setMaxSpeed(120);
            speedometer.setMajorTickStep(30);
            speedometer.setMinorTicks(4);

            // Configure value range colors
            speedometer.addColoredRange(0, 40, Color.GREEN);
            speedometer.addColoredRange(40, 80, Color.YELLOW);
            speedometer.addColoredRange(80, 120, Color.RED);

            ImageButton ib_call = (ImageButton) convertView.findViewById(R.id.ib_call);
            ImageButton ib_location = (ImageButton) convertView.findViewById(R.id.ib_location);

            tv_vehicle_number.setText(vehicleTrackingDetails.getVehicleNumber() == null ? "" : vehicleTrackingDetails.getVehicleNumber());

            if (vehicleTrackingDetails.getDriverName().equalsIgnoreCase("NA")) {
                tv_driver_name.setVisibility(View.INVISIBLE);
            } else {
                tv_driver_name.setVisibility(View.VISIBLE);
                tv_driver_name.setText(vehicleTrackingDetails.getDriverName());
            }

            speedometer.setSpeed(vehicleTrackingDetails.getSpeed() != null ? Integer.parseInt(vehicleTrackingDetails.getSpeed()) : 0);
            //  speedometer.setVisibility(View.GONE);
            tv_vehicle_speed.setText(vehicleTrackingDetails.getSpeed() != null ? vehicleTrackingDetails.getSpeed() + "kmph" : "0kmph");
            tv_digital_time.setText(vehicleTrackingDetails.getLastLocationDatetime() != null ? vehicleTrackingDetails.getLastLocationDatetime().substring(10, vehicleTrackingDetails.getLastLocationDatetime().length()) : "00:00:00");
            tv_digital_time.setTypeface(tf);
            tv_lastlocation_date.setText(vehicleTrackingDetails.getLastLocationDatetime() != null ? vehicleTrackingDetails.getLastLocationDatetime().substring(0, 10) : "NA");
            tv_lastlocation_city.setText(vehicleTrackingDetails.getLastLocation());
            tv_lastlocation_city.setPaintFlags(tv_lastlocation_city.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            img_vehicle_type.setImageResource(vehicleTrackingDetails.getIsNRD().equalsIgnoreCase("1") ? R.drawable.veh_stopped : vehicleTrackingDetails.getIsRunning().equalsIgnoreCase("1") ? (R.drawable.veh_moving) : (R.drawable.veh_nrd));
            Integer bl = new Integer(vehicleTrackingDetails.getBatteryLevel());
            img_battery_type.setImageResource(bl <= 0 ? (R.drawable.battery_low) : (bl > 0 && bl <= 11 ? (R.drawable.battery_medium) : (R.drawable.battery_high)));

            if(vehicleTrackingDetails.getIsStopped().equals("1")){
                tv_stoppage_time.setVisibility(View.VISIBLE);
                tv_stoppage_time.setText("Stoppage time - \n"+vehicleTrackingDetails.getStoppageDuration());
            }else {
                tv_stoppage_time.setVisibility(View.GONE);
            }

            final String phone = vehicleTrackingDetails.getDriverMobile();

            if (phone != null && !phone.equalsIgnoreCase("") && !phone.equalsIgnoreCase("null") && !phone.contains("NA")) {
                // tv_driver_mobile.setText("Driver Mobile : " + phone);
                //ib_call.setVisibility(View.VISIBLE);
            } else {
                //tv_driver_mobile.setText("Driver Mobile : NA ");
                ib_call.setVisibility(View.INVISIBLE);
            }

            final Double lat = vehicleTrackingDetails.getLatitude();
            final Double lang = vehicleTrackingDetails.getLongitude();


            ib_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (phone != null && !phone.equalsIgnoreCase("") && !phone.equalsIgnoreCase("null") && !phone.contains("NA")) {
                        Toast.makeText(context, "Making call to " + phone, Toast.LENGTH_SHORT).show();
                        //context.callDriver(phone);
                    } else {
                        Toast.makeText(context, "Sorry... Driver Number not available", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            tv_lastlocation_city.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (lat != null && lang != null) {
                        //context.showMap(lat, lang, vehicleTrackingDetails.getLastLocation());
                        try {
                            String location = "geo:0,0?q=" + lat + "," + lang + "(" + vehicleTrackingDetails.getLastLocation() + ")";
                            if (vehicleTrackingDetails.getLastLocation().equalsIgnoreCase("Location is unknown")) {
                                location = "http://maps.google.com/maps?q=" + lat + "," + lang;
                            }
                            Uri gmmIntentUri = Uri.parse(location);
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                            mapIntent.setPackage("com.google.android.apps.maps");
                            context.startActivity(mapIntent);
                        } catch (Exception e) {
                            Toast.makeText(context, "Google map in your phone is having issue to open", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "Sorry... Latitude or Longitude is not valid", Toast.LENGTH_SHORT).show();
                    }
                }
            });


            ib_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (lat != null && lang != null) {
                        //context.showMap(lat, lang,vehicleTrackingDetails.getLastLocation());
                        Intent intent = new Intent(context, TrailRouteActivity.class);
                        intent.putExtra(Constants.VEHICLE_DETAILS, vehicleTrackingDetails);
                        context.startActivity(intent);

                    } else {
                        Toast.makeText(context, "Sorry... Latitude or Longitude is not valid", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        return convertView;
    }
}
