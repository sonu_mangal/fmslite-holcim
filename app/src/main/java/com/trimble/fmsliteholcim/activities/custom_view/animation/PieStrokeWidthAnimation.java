package com.trimble.fmsliteholcim.activities.custom_view.animation;

/**
 * Created by sbhosal on 10/18/17.
 */

import android.view.animation.Animation;
import android.view.animation.Transformation;

import com.trimble.fmsliteholcim.activities.custom_view.PieView;

/**
 * @author Alejandro Zürcher (alejandro.zurcher@gmail.com)
 */
public class PieStrokeWidthAnimation extends Animation {

    private PieView arcView;
    private int oldWidth;

    public PieStrokeWidthAnimation(PieView arcView) {
        this.oldWidth = arcView.getPieInnerPadding();
        this.arcView = arcView;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation transformation) {
        int width = (int)(oldWidth * interpolatedTime);
        arcView.setPieInnerPadding(width);
        arcView.requestLayout();
    }
}